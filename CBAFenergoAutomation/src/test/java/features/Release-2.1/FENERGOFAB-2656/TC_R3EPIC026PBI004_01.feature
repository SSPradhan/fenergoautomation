#Test Case: TC_R3EPIC026PBI004_01
#PBI:R3EPIC026PBI001
#AC ID:AC-1,AC-2,AC-3
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R3EPIC026PBI004_01

  Scenario: NBFI -Validate the field behavior for field "Agent performing due diligence for any underlying principals?" under section "DTCC Eligibility" on screen "Capture DTTC Letter" in Agency Work Flow  
  # Validate "document requirements" should trigger if user select option "yes" for field "Agent performing due diligence for any underlying principals?" on screen "Capture DTTC letter"
  # Validate "document requirements" should not trigger if user select option "NO" for field "Agent performing due diligence for any underlying principals?" on screen "Capture DTTC letter"
  # Validate 10 new DTCC Alert document type  should be added under section"Document Requirements" and 1st  DTCC Alert document should be  mandatory and others(9) are non Mandatory on screen 
  # Validate default values for fields (Document Name,Document Category,Document Type and Expiration Date)  on screen "Document Details"
  # Validate the task name "Complete Fulfil Reliance Letter Doc Requirement" should be renamed as "Complete Fulfil DTCC Letter Doc Requirement" 
  # Validate error message "Cannot save and complete as all the document requirements have not been met" should appear on screen if  Document Requirement  Status is "In Progress" on screen "Capture DTTC Letter "
  ########################################################################################################################
   #PreCondition: Create entity with client type as NBFI and confidential as NBFI 
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "AssociationDetails" screen
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: NBFI"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
 	
    # Initiate Agency Request
    Given I login to Fenergo Application with "GMO BO Maker / RM: NBFI "
    When I search for the "CaseId"
    When I navigate to LE 360 screen
    And I initiate  "Agency Request"  from action button
   	When I navigate to "Request Details: Select Trading Agreement " task
  	When I complete "Request Details: Select Trading Agreement " task
  	When I navigate to "CaseDetails" screen
  	# Validate the task name "Complete Fulfil Reliance Letter Doc Requirement" should be renamed as "Complete Fulfil DTCC Letter Doc Requirement" 
  	And I verify the task name 
  	When I navigate to "Request Details: Complete Fulfil  DTCC Letter Doc Requirements" task
   	#Validate the field behavior for field "Agent performing due diligence for any underlying principals?" under section "DTCC Eligibility" on screen "Capture DTTC Letter"
   	And I Verify the field behaviour for above mentioned field
      | FieldLabel                                                    | Field Type | Mandatory | Editable | Field Defaults to | Visible  | 
      | Agent performing due diligence for any underlying principals? | Dropdown   | Yes       | Yes      |Select...          | Yes      | 
   	
   	# Validate "document requirements" should not trigger if user select option "yes" for field "Agent performing due diligence for any underlying principals?" on screen "Capture DTTC letter"
   	And I select "No" for the field "Agent performing due diligence for any underlying principals?"
   	And I click on "Save and Update" button
   	And I verify "document requirements" section is not appaering on screen
   	# Validate "document requirements" should trigger if user select option "yes" for field "Agent performing due diligence for any underlying principals?" on screen "Capture DTTC letter"
   	And I select "yes" for the field "Agent performing due diligence for any underlying principals?"
   	And I click on "Save and Update" button
   	And I verify "document requirements" section is appaering on screen 
   	# Validate 10 new DTCC Alert document type  should be added under section "Document Requirements" and 1st  DTCC Alert document should be  mandatory and others(9) are non Mandatory on screen
   	And I verify 10 new doc DTCC Alert documnt type is appearing under section "Document Requirements" and 1st  DTCC Alert document should be  mandatory and others(9) are non Mandatory on screen
  	 # Validate error message "Cannot save and complete as all the document requirements have not been met" should appear on screen if  Document Requirement  Status is "In Progress" on screen "Capture DTTC Letter "
  	And I click on "Save and Complete" button
  	And I verify the above mentioned error message is appearing on screen
  	And I select "Attach Document" from ... button against document type "DTCC Alert Doc"
  	When I navigate to "Document Details" screen
  	# Validate default values for fields (Document Name,Document Category,Document Type and Expiration Date)  on screen "Document Details"
  	And I verify the deafult values for above mentioned field
  	|FieldLabel       |Field Default to|
  	|Document Category|MISC            |
  	|Document Type    |DTCC Alert      |
  	|Document Name    |DTCC Alert      |
  	|Expiration Date  |31-12-2069      |
  
  	
  	And I upload the document under section "Document Requirement"
  	And I click on "Save and Complete" button
  	When I complete "Request Details: Complete Fulfil  DTCC Letter Doc Requirements" task
  	
  	
  	
  	
    
    
    
    
    
    
    