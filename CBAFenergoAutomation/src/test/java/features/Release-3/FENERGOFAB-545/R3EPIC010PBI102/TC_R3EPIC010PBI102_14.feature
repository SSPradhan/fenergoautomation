  #Test Case: TC_R3EPIC010PBI102_14
  #PBI: R3EPIC010PBI102
  #User Story ID: Reject / Refer / Approve 01 - 05
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @COB
  Scenario: FI - Validate field behaviour for field 'REFER REASON' in "Review and Approval" stage in COB workflow
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer" 
    # Validate field 'REFER REASON' should be come between field "Refer to Stage" and Refer Reasom Comment" on under section "Manager Review" on 8 Manager Review & Approval screens
    # Validate 'REFER REASON'  LoVs are displayed in the order mentioned in the PBI-LOV tab(Total LOV = 20 and New Lov = 10 new 'Reject' values)
    # Validate "Relationship Manager" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    # Validate "CIB R&C KYC Approver - KYC Managers" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "CIB R&C KYC Approver - AVP" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "CIB R&C KYC Approver - VP" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "CIB R&C KYC Approver - SVP" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "Group Compliance (CDD)" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "Business Unit Head" user role group should able to add refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
   # Validate "Business Head" user role group should able to add refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    ##################################################################################################
    #PreCondition: Create entity with client type as FI and confidential as FI and RiskRating should be very high
    
    #######################################################################################
   
    Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FIG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I navigate to "Classification" task
    When I complete "Classification" task    
   #########################################################################################
    When I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "Relationship Manager Review and Sign-Off" task
    #  Screen "Relationship Manager Review and Sign-Off "
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "Relationship Manager" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
    When I complete "Relationship Manager Review and Sign-Off" task
    And I verify "Relationship Manager" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons
   ##########################################################################################
   When I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #  Screen "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" 
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "CIB R&C KYC Approver - KYC Managers" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task  
     And I verify "CIBR&CKYCApprover-KYCManager" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
   #####################################################################################################
    When I login to Fenergo Application with "CIB R&C KYC Approver - AVP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - AVP Review and Sign-Off"  task
    #  Screen "CIB R&C KYC Approver - AVP Review and Sign-Off" 
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "CIB R&C KYC Approver - AVP " user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     And I verify "CIB R&C KYC Approver - AVP" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    
    ####################################################################################################################
    When I login to Fenergo Application with "CIB R&C KYC Approver - VP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - VP Review and Sign-Off"  task
    #  Screen "CIB R&C KYC Approver - VP Review and Sign-Off" 
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "CIB R&C KYC Approver - VP " user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - VP Review and Sign-Off" task
     And I verify "CIB R&C KYC Approver - VP" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    ########################################################################################################################
    When I login to Fenergo Application with "CIB R&C KYC Approver - SVP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - SVP Review and Sign-Off"  task
    #  Screen "CIB R&C KYC Approver - SVP Review and Sign-Off" 
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "CIB R&C KYC Approver - SVP " user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     And I verify CIB R&C KYC Approver - SVP" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    #########################################################################################################################
    When I login to Fenergo Application with "Group Compliance (CDD)"
    When I search for the "CaseId"
    When I navigate to "Group Compliance (CDD) Review and Sign-Off"  task
    #  Screen "Group Compliance (CDD) Review and Sign-Off"
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "Group Compliance (CDD)" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I navigate to "Group Compliance (CDD) Review and Sign-Off" task
     when I complete "Group Compliance (CDD) Review and Sign-Off" task
     And I verify "Group Compliance (CDD)" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    ###########################################################################################################################
    When I login to Fenergo Application with "Business Unit Head"
    When I search for the "CaseId"
    When I navigate to "Business Unit Head Review and Sign-Off"  task
    #  Screen "Business Unit Head Review and Sign-Off"
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "Business Unit Head" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I navigate to "Group Compliance (CDD) Review and Sign-Off" task
     when I complete "Group Compliance (CDD) Review and Sign-Off" task
     When I navigate to "Business Unit Head Review and Sign-Off"
     When I complete "Business Unit Head Review and Sign-Off" task
    And I verify "Business Unit Head" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    #############################################################################################################################
     
    When I login to Fenergo Application with "Business Head"
    When I search for the "CaseId"
    When I navigate to "Business Head Review and Sign-Off"  task
    #  Screen "Business Head Review and Sign-Off"
    # Validate field 'REFER REASON' should be mandatory and visible on screen when "Review Outcome" type lov is selected as "Refer"  
    And I select "Refer" for "Review Outcome" field
    And I verify 'Refer Reason' field is became visible and mandatory 
    # Validate field 'Refer Reason' should be come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review" 
    And I verify field 'Refer Reason' come between field "Refer to Stage" and Refer Reason Comment" on under section "Manager Review"
    # Validate field behavior for field 'REFER REASON'  
     And I validate field behaviour for field 'REFER REASON' under "Manager Review" section
      | Label          | Field Type               | Visible | Editable | Mandatory  | Field Defaults To |
      | Refer Reason   |  Multi select Drop-down  | Yes     | Yes      | Yes        |   Select...        |
     # Validate below mentioned LoVs are displayed for field "Refer Reason" in the order mentioned in the PBI-LOV tab
    | Adverse Media Not Identified / Assessed                                   |
    | PEP Not Identified / Assessed                                             |
    | Sanctions Nexus Not Identified / Assessed                                 |
	| Ownership Structure Not Clear / Incomplete                                |
	| Name Screening Incorrect / Incomplete                                     |
	| Incorrect Risk Rating                                                     |
	| Insufficient / Incorrect Documentation Provided                           |
	| Insufficient / Incorrect Information Provided                             |
	| Additional Information / EDD Required                                     |
	| Others                                                                    |
	| Reject: Prohibited as per Group AML/ CTF policy                           |
	| Reject: Prohibited as per Group Sanctions Policy                          |
	| Reject: Declined due to AML concerns                                      |
	| Reject: Declined due to Sanctions concerns                                |
	| Reject: Declined due to Fraud Concerns                                    |
	| Reject: Declined due to Material Adverse Media Related to Financial Crime |
	| Reject: Declined due to Bribery & Corruption Concerns                     |
	| Reject: Rejected due to incomplete documents                              |
	| Reject: Rejected due to Central bank blacklisting                         |
	| Reject: Others                                                            |
      And I validate LOVs of "Refer Reason" field
     #Refer PBI for LOV list   
     # Validate "Business  Head" user role group should able to refer a case back to a previous stage of the COB workflow with a specific "Reject" reason or reasons
    When I select "Risk Assessment" for field "RefertoStage"
    when I select a value for field "Refer Reason"
    When I give comment for field "Refer Reason Comments"
    And I click on "Submit" button
    Then I see "Risk Assessment" task is generated
    When I navigate to "Risk Assessment" task
    When I complete "Risk Assessment" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
    When I navigate to "Relationship Manager Review and Sign-Off" task
     When I complete "Relationship Manager Review and Sign-Off" task
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task   
     When I naviagate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - AVP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - VP Review and Sign-Off" task
     When I naviagate to "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I complete "CIB R&C KYC Approver - SVP Review and Sign-Off" task
     When I navigate to "Group Compliance (CDD) Review and Sign-Off" task
     when I complete "Group Compliance (CDD) Review and Sign-Off" task
     When I navigate to "Business Unit Head Review and Sign-Off"
     When I complete "Business Unit Head Review and Sign-Off" task
     When I navigate to "Business Head Review and Sign-Off"
     When I complete "Business Head Review and Sign-Off" task 
     And I verify "Business Head" user role group is able to refer back to a previuos stage of the COB workflow with a specific "Reject" reason or reasons 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   