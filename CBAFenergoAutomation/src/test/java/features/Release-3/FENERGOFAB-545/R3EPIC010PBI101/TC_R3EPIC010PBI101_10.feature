  #Test Case: TC_R3EPIC010PBI101_10
  #PBI: R3EPIC010PBI101
  #User Story ID: Reject/Refer/Approve 06,Reject/Refer/Approve07,Reject/Refer/Approve 06+07
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @COB
  Scenario: BBG - Validate field behaviour for field 'Additional Status' in "Review and Approval" stage in RR workflow
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    # Validate field 'Additional Status' should be come after field "Review Outcome" on under section "Manager Review" on 8 Manager Review & Approval screens
    # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
    # Validate default value should be 'Approved' for field 'Additional Status' for Relationship Manager,Business Unit Head and  Business Head user role group
    # Validate default value should be 'Select...' for field 'Additional Status' for CIB R&C KYC Approver - KYC Managers,CIB R&C KYC Approver - AVP,CIB R&C KYC Approver - VP,CIB R&C KYC Approver - SVP and Group Compliance (CDDuser role group
    # Validate "Relationship Manager" user role group should able to add comment in field "Review Notes" on screen "Relationship Manager Review and Sign-Off"
    # Validate "CIB R&C KYC Approver - KYC Managers" user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - KYC Managers Review and Sign-Off"
   # Validate "CIB R&C KYC Approver - AVP" user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - AVP Review and Sign-Off"
   # Validate "CIB R&C KYC Approver - VP" user role group should able to add comment in field "Review Notes"  on screen " CIB R&C KYC Approver - VP Review and Sign-Off"
   # Validate "CIB R&C KYC Approver - SVP" user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - SVP Review and Sign-Off"
   # Validate "Group Compliance (CDD)" user role group should able to add comment in field "Review Notes" on screen "Group Compliance (CDD) Review and Sign-Off"
   # Validate "Business Unit Head" user role group should able to add comment in field "Review Notes"on screen "Business Unit Head Review and Sign-Off"
   # Validate "Business Head" user role group should able to add comment in field "Review Notes" on screen "Business Head Review and Sign-Off"
    ##################################################################################################
    #PreCondition: Create entity with client type as BBG and confidential as BBG and RiskRating should be very high
    #PreCondition:  Risk Rating should be very High in RR
    #######################################################################################
   
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I navigate to "Classification" task
    When I complete "Classification" task    
    When I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    ##########################################################################################3
     # Initiate RR workflow
     And I initiate "Regular Review" from action button
     And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
	When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "Preliminary Tax Assessment" task
    And I select "Yes" for the field "Do you have a US Tax Form from the client?"
    When I complete the "Preliminary Tax Assessment" task	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	And I click on "ID&VLinkInRR" button 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
   #########################################################################################
    When I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "Relationship Manager Review and Sign-Off" task
    #  Screen "Relationship Manager Review and Sign-Off "
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "Relationship Manager Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen "Relationship Manager Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type  | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |             | Yes     | No       | Yes        |   Approved        | 
     # Validate "Relationship Manager" user role group should able to add comment in field "Review Notes" on screen "Relationship Manager Review and Sign-Off"
     When I add comment for field "Review Notes"
     And I verify user is able to add comment in field "Review Notes" on screen "Relationship Manager Review and Sign-Off"
     When I complete "Relationship Manager Review and Sign-Off" task
   ##########################################################################################
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    # Screen CIB R&C KYC Approver - KYC Managers Review and Sign-Off
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "CIB R&C KYC Approver - KYC Managers Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen "CIB R&C KYC Approver - KYC Managers Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type       | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |  Multi Select    | Yes     | Yes      | Yes        |   Select...        | 
      # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
      And I validate LOVs of "Additional Status" field
     #Refer PBI for LOV list  
     # Validate "CIB R&C KYC Approver - KYC Managers" user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - KYC Managers Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - KYC Managers Review and Sign-Off"
    When I complete "CIB R&C KYC Approver - KYC Managers Review and Sign-Off" task
   #####################################################################################################
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - AVP Review and Sign-Off" task
    # Screen "CIB R&C KYC Approver - AVP Review and Sign-Off
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "CIB R&C KYC Approver - AVP Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen CIB R&C KYC Approver - AVP Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type       | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |  Multi Select    | Yes     | Yes      | Yes        |   Select...        | 
      # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
      And I validate LOVs of "Additional Status" field
     #Refer PBI for LOV list  
     # Validate ""CIB R&C KYC Approver - AVP " user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - AVP Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - AVP Review and Sign-Off"
    When I complete CIB R&C KYC Approver - AVP Review and Sign-Off" task
    ####################################################################################################################
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - VP Review and Sign-Off" task
    # Screen "CIB R&C KYC Approver - VP Review and Sign-Off
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "CIB R&C KYC Approver - VP Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen CIB R&C KYC Approver - VP Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type       | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |  Multi Select    | Yes     | Yes      | Yes        |   Select...        | 
      # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
      And I validate LOVs of "Additional Status" field
     #Refer PBI for LOV list  
     # Validate ""CIB R&C KYC Approver - VP " user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - VP Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - VP Review and Sign-Off"
    When I complete CIB R&C KYC Approver - VP Review and Sign-Off" task
    ########################################################################################################################
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - SVP"
    When I search for the "CaseId"
    When I navigate to "CIB R&C KYC Approver - SVP Review and Sign-Off" task
    # Screen "CIB R&C KYC Approver - SVP Review and Sign-Off
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "CIB R&C KYC Approver - SVP Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen CIB R&C KYC Approver - SVP Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type       | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |  Multi Select    | Yes     | Yes      | Yes        |   Select...        | 
      # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
      And I validate LOVs of "Additional Status" field
     #Refer PBI for LOV list  
     # Validate ""CIB R&C KYC Approver - SVP " user role group should able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - SVP Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "CIB R&C KYC Approver - SVP Review and Sign-Off"
    When I complete CIB R&C KYC Approver - SVP Review and Sign-Off" task
    #########################################################################################################################
    Then I login to Fenergo Application with "Group Compliance (CDD)"
    When I search for the "CaseId"
    When I navigate to "Group Compliance (CDD) Review and Sign-Off" task
    # Screen "Group Compliance (CDD) Review and Sign-Off"
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "Group Compliance (CDD) Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen "Group Compliance (CDD) Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type       | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |  Multi Select    | Yes     | Yes      | Yes        |   Select...        | 
      # Validate 'Additional Status'  LoVs are displayed in the order mentioned in the PBI-LOV tab
      And I validate LOVs of "Additional Status" field
     #Refer PBI for LOV list  
     # Validate "Group Compliance (CDD) " user role group should able to add comment in field "Review Notes" on screen "Group Compliance (CDD) Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "Group Compliance (CDD) Review and Sign-Off"
    When I complete "Group Compliance (CDD) Review and Sign-Off"
    ###########################################################################################################################
    Then I login to Fenergo Application with "Business Unit Head"
    When I search for the "CaseId"
    When I navigate to "Business Unit Head Review and Sign-Off" task
    # Screen "Business Unit Head Review and Sign-Off" 
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "Business Unit Head Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen "Business Unit Head Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type  | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |             | Yes     | No       | Yes        |   Approved        | 
     # Validate "Business Unit Head" user role group should able to add comment in field "Review Notes" on screen "Business Unit Head Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "Business Unit Head Review and Sign-Off"
    When I complete "Business Unit Head Review and Sign-Off"
    #############################################################################################################################
     Then I login to Fenergo Application with "Business  Head"
    When I search for the "CaseId"
    When I navigate to "Business Head Review and Sign-Off" task
    # Screen "Business  Head Review and Sign-Off" 
    # Validate field 'Additional Status' should be mandatory on screen when "Review Outcome" type lov is selected as "Approve" 
    And I select "Approved" for "Review Outcome" field
    And I verify 'Additional Status' filed is became mandatory
    # Validate field 'Additional Status' should be come after field "Review Outcome" on screen "Business Head Review and Sign-Off"
    And I verify field 'Additional Status' come after field "Review Outcome" on screen "Business Head Review and Sign-Off"
    # Validate field behavior for field 'Additional Status'   
     And I validate field behaviour for field 'Additional Status' under "Manager Review" section
      | Label              | Field Type  | Visible | Editable | Mandatory  | Field Defaults To |
      | Additional Status  |             | Yes     | No       | Yes        |   Approved        | 
     # Validate "Business Unit Head" user role group should able to add comment in field "Review Notes" on screen "Business Head Review and Sign-Off"
      When I add comment for field "Review Notes"
      And I verify user is able to add comment in field "Review Notes" on screen "Business  Head Review and Sign-Off"
    When I complete "Business  Head Review and Sign-Off"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   