#Test Case: TC_R2S3EPIC018PBI001.2_03
#PBI: R2S3EPIC018PBI001.2
#User Story ID: US13
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S3EPIC018PBI001.2_03

  Scenario: Validate KYC Maker is able to Refer the LEM case from 'risk Assessment' stage to "Maintenance request" stage for LEM Workflow (Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    #Validate KYC Maker is able to Refer the LEM case from 'Review and Approval' stage to 'Maintenance request' stage for LEM Workflow (Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    #Validate KYC Maker is able to Refer the LEM case from 'Review and Approval' stage to "Materiality review" stage for LEM Workflow (Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    #	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate"
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Validate KYC Maker is able to Refer the LEM case from 'risk Assessment' stage to "Maintenance request" stage for LEM Workflow
    #(Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    Then I validate "CompleteRiskAssessment" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'refer' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'refer' option
    #Validate user is navigate to 'case referral' task screen and 'referral reason' textbox is displaying as mandatory
    When I navigate to 'Casereferral' task screen
    Then I validate 'Referralreason' textbox is displaying as mandatory
    When I select 'Refertostage' as 'Maintenancerequest' stage and add 'Referralreason' and click on 'refer' button
    #validate case is referred to maintenance request stage
    #validate maintenance request stage is generated and status is displaying as 'In Progress' in Task grid
    Then I see 'CaptureProposedChanges' task is generated in Tasks grid
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Validate KYC Maker is able to Refer the LEM case from 'Review and Approval' stage to 'Maintenance request' stage for LEM Workflow
    #(Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    Then I validate "CIBR&CKYCApproverAVPReviewGrid" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'refer' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'refer' option
    #Validate user is navigate to 'case referral' task screen and 'referral reason' textbox is displaying as mandatory
    When I navigate to 'Casereferral' task screen
    Then I validate 'Referralreason' textbox is displaying as mandatory
    When I select 'Refertostage' as 'Maintenancerequest' stage and add 'Referralreason' and click on 'refer' button
    #validate case is referred to maintenance request stage
    #validate maintenance request stage is generated and status is displaying as 'In Progress' in Task grid
    Then I see 'CaptureProposedChanges' task is generated in Tasks grid
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Validate KYC Maker is able to Refer the LEM case from 'Review and Approval' stage to "Materiality review" stage for LEM Workflow
    #(Also, Validate 'referral reason' section display as mandatory as per DD on case Referral screen)
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    Then I validate "CIBR&CKYCApproverAVPReviewGrid" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'refer' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'refer' option
    #Validate user is navigate to 'case referral' task screen and 'referral reason' textbox is displaying as mandatory
    When I navigate to 'Casereferral' task screen
    Then I validate 'Referralreason' textbox is displaying as mandatory
    When I select 'Refertostage' as 'Materialityreview' stage and add 'Referralreason' and click on 'refer' button
    #validate case is referred to Materiality review stage
    #validate Materiality review stage is generated and status is displaying as 'In Progress' in Task grid
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
