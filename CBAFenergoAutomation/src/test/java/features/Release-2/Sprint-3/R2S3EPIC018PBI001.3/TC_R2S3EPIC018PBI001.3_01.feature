#Test Case: TC_R2S3EPIC018PBI001.3_01
#PBI: R2S3EPIC018PBI001.3
#User Story ID: US01, US15
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S3EPIC018PBI001.3_01

  Scenario: Validate 'Account Service maker' is able to trigger/initiate LEM case only for those entities/clients whose Client onboarding is completed
    #Validate users except 'Account Service maker' should not be able to trigger LEM case on LE360 screen.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360*
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    #	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate"
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    #validate user is not able to Initiate LEM case
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the 'CaseID'
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #validate user is able to Initiate LEM case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is appearing along with other options
    When I click on 'MaintenanceRequest' option
    Then I navigate to "Maintenancerequest" task
    #Validate LEM case is triggered successfully
    When I Select Area as "LEdetails", LE Details changes as "Relationships" and add "Reason" and click on 'Submit'
    #Validate Capture proposed changes task is generated sucessfully
    And I validate Capture proposed changes task is generated sucessfully
    #validate KYC maker is not able to Initiate LEM case
    Given I login to Fenergo Application with "KYCmaker"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    #validate KYC Manager is not able to Initiate LEM case
    Given I login to Fenergo Application with "KYCmanager"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    #validate AVP is not able to Initiate LEM case
    Given I login to Fenergo Application with "AVP"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    #validate VP is not able to Initiate LEM case
    Given I login to Fenergo Application with "VP"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    #validate CDD is not able to Initiate LEM case
    Given I login to Fenergo Application with "CDD"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
    #validate Business Unit Head is not able to Initiate LEM case
    Given I login to Fenergo Application with "BUH_IBGDNE"
    When I search for the 'CaseID'
    When I navigate to "LE360Ledetails" task and click on Actions button
    Then I validate 'MaintenanceRequest' option is not appearing along with other options
