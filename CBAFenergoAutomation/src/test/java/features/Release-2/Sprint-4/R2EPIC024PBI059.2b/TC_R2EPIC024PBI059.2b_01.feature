#Test Case: TC_R2EPIC024PBI059.2b_01
#PBI:R2EPIC024PBI059.2b
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2b_01

Scenario: Validate 'Legal Entity Category' field is not visible in COB workflow.
    Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I Navigate "NewRequest" screen with key "Corporate"
     When I Navigate "Complete" screen
     Validate 'LE Category' not visible on Complete screen
     Then I validate 'LE Category' field is not-visible
     Then I validate 'LE Category' field is un-editable
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
     Validate 'LE Category' not visible on Capture Request Details screen
     Then I validate 'LE Category' field is not-visible
     Then I validate 'LE Category' field is un-editable
      And I click on "Continue" button
     When I Navigate "Review Request" screen 
     Validate 'LE Category' not visible on under Complete screen
	 Then I validate 'LE Category' field is not-visible
     Then I validate 'LE Category' field is un-editable
     When I complete "ReviewRequest" task
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task
     Validate 'LE Category' not visible on Validate KYC and Regulatory Data screen
     Then I validate 'LE Category' field is not-visible
     Then I validate 'LE Category' field is un-editable
     When I complete "ValidateKYC" screen with key "C1" 
     And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
     Validate 'LE Category' field not visible on Enrich KYC Profile screen 
     Then I validate 'LE Category' field is not-visible
     Then I validate 'LE Category' field is un-editable
      