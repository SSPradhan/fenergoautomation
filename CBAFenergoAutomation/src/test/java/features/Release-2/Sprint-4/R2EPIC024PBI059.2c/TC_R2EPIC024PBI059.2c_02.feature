#Test Case: TC_R2EPIC024PBI059.2c_02
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2c_02

Scenario: Validate the logic for the new 'LE Category' field applied for an already completed COB case and the field value refreshed upon 
		  completion of the first task in LE Maintenance case but field is not visible.

#Pre-Requisite : An already COB case.
	
	Given I login to Fenergo Application with "SuperUser" 
	When I search for the "CaseId" 
	And I initiate "Maintenance Request" from action button 
	When I select "LE Details" for field "Area" 
	When I select "Area - KYC Data and Customer Details" for field "LE Details Changes" 
	And I click on "CreateLEMSubmit" button 
	And I complete "MaintenanceRequest" screen with key "KYCData" 
	Then I store the "CaseId" from LE360 
	Then I see "CaptureProposedChangesGrid" task is generated 
	And I navigate to "CaptureProposedChangesGrid" task
	When I complete "CaptureProposedChanges" screen with key "LowRisk" 
	And I click on "SaveandCompleteCaptureProposedChanges" button 
	
	#After completion of first task in LE-Maintenance i.e. Capture Proposed Changes
	#Open the COB on which above LEM was triggered

	 Given I login to Fenergo Application with "RM:IBG-DNE"	
	 When I search for the "CaseId" of already completed COB case.
     Then I store the "CaseId" from LE360       
     When I Navigate "Capture Request Details" screen
	 Validate 'LE Category' field is not visible on Capture Request Details screen
     Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I Navigate "Review Request" screen 
     Validate 'LE Category' not visible on Complete screen
     Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "ValidateKYCandRegulatoryGrid" task
     Validate 'LE Category' not visible on Validate KYC and Regulatory Data screen
     Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "EnrichKYCProfileGrid" task   
     Validate 'LE Category' field not visible on Enrich KYC Profile screen 
     Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable