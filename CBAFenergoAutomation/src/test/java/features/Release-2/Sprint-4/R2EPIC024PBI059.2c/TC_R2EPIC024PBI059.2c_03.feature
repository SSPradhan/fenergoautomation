#Test Case: TC_R2EPIC024PBI059.2c_03
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2c_03

Scenario: Validate the logic for the new 'LE Category' field applied for an already completed COB case and the field value refreshed upon 
		  completion of the first task in Regular Review case but field is not visible.

#Pre-Requisite : An already COB case.

	Given I login to Fenergo Application with "SuperUser" 
	When I search for the "CaseId" 
	
	And I initiate "Regular Review" from action button 
       When I navigate to "CloseAssociatedCaseGrid" task 
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     Then I store the "CaseId" from LE360 
     When I complete "ValidateKYC" screen with key "C1"
     And I click on "SaveandCompleteforValidateKYC" button 
      
    #After completion of first task in Regular Review i.e. Validate KYC and Regulatory Data
	#Open the COB on which above RR was triggered
     
    Given I login to Fenergo Application with "RM:IBG-DNE"	
	 When I search for the "CaseId" of already completed COB case.
     Then I store the "CaseId" from LE360       
     When I Navigate "Capture Request Details" screen
     Validate 'LE Category' is not visible on Capture Request Details screen
	 Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I Navigate "Review Request" screen 
     Validate 'LE Category' is not visible on under Complete screen
     Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "ValidateKYCandRegulatoryGrid" task
     Validate 'LE Category' is not visible on Validate KYC and Regulatory Data screen
	 Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "EnrichKYCProfileGrid" task   
     Validate 'LE Category' field is not visible on Enrich KYC Profile screen 
	 Then I validate 'LE Category' field not visible
     Then I validate 'LE Category' field is un-editable