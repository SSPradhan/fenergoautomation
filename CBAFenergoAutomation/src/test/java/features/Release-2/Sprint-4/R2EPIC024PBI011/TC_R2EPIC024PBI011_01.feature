#Test Case: TC_R2EPIC024PBI011_01
#PBI:R2EPIC024PBI011
#User Story ID:Back_Log_001
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC024PBI011_01

Scenario: Validate for RM user 'DAO code' drop-down is displaying as Multi-select drop-down on User management screen
#Validate RM user is able to add Multiple 'DAO code' to single RM on user management screen

 Given I login to Fenergo Application with "Admin"
 When I click on options button and click 'security' option
 When I navigate to 'user management' grid by clicking on 'user management' option 
 When I expand 'Advanced search' grid
 When I select Role group as 'relationship Manager'
 #Verify DAO code drop-down is displaying as mandatory
 Then I see DAO code drop-down is diaplying as mandatory
 #verify 'DAO code' drop-down is displaying as Multi-select drop-down
 And I assert that 'DAO code' drop-down is displaying as Multi-select
 #Validate user is able to add Multiple 'DAO code' to single Relationship manager
 When I select values from 'DAO code' drop-down
 Then I Validate user is able to add Multiple 'DAO code' to single Relationship manager
 And I save the details