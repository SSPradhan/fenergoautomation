  #Test Case: TC_R2EPICCRPBI001_04
  #PBI: R2EPICCRPBI001
  #User Story ID: Data_ADD_001
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @COB
  Scenario: Corporate(in-flight case) -Validate when the user refer back to the Addresses subflow, a validation message should be displayed ,if the length of the character is more than 35 & other than specified special characters in "New request"" Stage
   #Validate the maximum length for the fields "Address line 1", "Address line 2", and "Town/ city" should be 35 characters
   #Validate only "  " " ' ( ) + , - . / : ? "special characters are allowed for the fields "Address line 1", "Address line 2", and "Town/ city"
   #validate "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #Validate When referring back to New Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
   #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
   #Internal Booking Details section - No change (OOTB feature)
    #Customer Details section - No change (OOTB feature)
    #Relationship section - No change (OOTB feature)
    ##################################################################################################
    #PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE with Address more than 35characters and special characters as well
    #Case ID: 8047
    #######################################################################################
   
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    When I naviagte to "CaptureRequestDetails" task
    And I click on(+) against "Addresses subflow""
    #Address screen
    And I Select Address Type as "Registered COI"
    When I write "Address-12345678("Residentadress1")####@@@@@Adresss234"  for field "Address line 1"
    When I write "Address-12345678("Residentadress2")####@@@@@Adresss234" for field "Address line 2"
    When I write "Address-12345678("Residenttown")####@@@@@Adresss234" for field "Town/ city"
    And I click on "Save " button
    And I complete "CaptureRequestDetails" task by clicking "Continue" button
    when I navigate to "ReviewRequest" task
    And I Validate all the updated values are retained in "ReviewRequest" screen
    When I complete "ReviewRequest" task
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
      
    #Verify When referring back to New Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
    When I select "Refer" from action button
    When I select "New Request" for field "RefertoStage"
    when I write "Test" for field "Referral Reason"
    And I click on "Refer" button
    Then I see "Capture Request Details" task is generated
    When I navigate to "Capture Request Details" task
    And I select "Edit" option from the "Action" button on "Addresses" subflow
    When I navigate to "Address " screen
    
    And I Validate all the updated values are retained in "Address" screen
    #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
    And I verify "Save and Add another" & "Save" button is not enabled
    #Verify a validation message should be displayed ,if F the length of the character is more than 35 & other than specified special characters
    And I Valiate validation message is appearing on screen for the fields "Address line 1", "Address line 2", and "Town/ city" "
    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for field "Address 1"
    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for the field "Address 2"
     And I validate the validation message "You've reached the maximum length.Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for the field "Town/ City"
    
    