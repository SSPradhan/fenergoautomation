#Test Case: TC_WAC_Corporate_05
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_05
@Automation
  Scenario: RR Refer back -Derive the overrall risk rating as High (COB overall risk rating is Medium)
    #Refer to TC25 in the WAC Corp Data Sheet
    
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    When I complete "Product" screen with key "FiduciaryAccount"
    And I complete "CaptureNewRequest" with Key "MediumRiskCorporate" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "MediumRiskCorporate"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "MediumRiskCorporate"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "SanctionsSSIOnly"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I verify the populated risk rating is "Medium"
    When I complete "RiskAssessment" task
    
     Then I login to Fenergo Application with "RM:IBG-DNE"
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360
     When I navigate to "ReviewSignOffGrid" task
     When I complete "ReviewSignOff" task
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
     When I search for the "CaseId"
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "ReviewSignOff" task
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
     When I search for the "CaseId"
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
     When I complete "ReviewSignOff" task
  
     Then I login to Fenergo Application with "BUH:IBG-DNE"
     When I search for the "CaseId"
     When I navigate to "BHUReviewandSignOffGrid" task
     When I complete "ReviewSignOff" task
  
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    #Comment: = RegularReview
    And I initiate "Regular Review" from action button
    
    When I complete "CloseAssociatedCase" task
    
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I store the "CaseId" from LE360
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "ReviewRequestGrid" task
    When I complete "RRReviewRequest" task
    
    When I navigate to "Review/EditClientDataTask" task
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I do "DocumentUpload" for all pending documents
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I verify the populated risk rating is "Medium"
    
    And I click on "RegularReviewCaseDetails" button
    And I refer back the case to "Review Client Data" stage
    
    When I complete "CloseAssociatedCase" task
    
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I store the "CaseId" from LE360
    When I complete "ValidateKYC" screen with key "MediumRiskCorporate"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "ReviewRequestGrid" task
    When I complete "RRReviewRequest" task
    
    When I navigate to "Review/EditClientDataTask" task
    When I complete "EnrichKYC" screen with key "TC_WAC_Corporate_05"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I do "DocumentUpload" for all pending documents
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "SanctionsSSIOnly"
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I verify the populated risk rating is "High"
    When I complete "RiskAssessment" task
    

    
