#Test Case: TC_WAC_Corp_RR_03
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corp_RR_03

  Scenario: RR-Derive the overrall risk rating as Medium-Low (COB overall risk rating is High)
    #Refer to TC23 in the WAC Corp Data Sheet