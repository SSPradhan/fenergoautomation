#Test Case: TC_R2EPIC013PBI002_01
#PBI: R2EPIC013PBI002
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: GLCMS

  Scenario: Validate below fields as per DD(Editable, Visible, Mandatory, Defaults to..) on  "Waiting for UID from GLCMS" task "Capture FAB references" stage
    #Scenario: Validate "Status" field is auto-populated as "processed" once Fenergo receives "GLCMS UID" and "GLCMS UID" is
    # auto-populated for "Capture Fab references" task
    #Scenario: Verify Resend button display as disabled for KYC Maker on "Waiting for UID from GLCMS" task when "Status" field is auto-populated
    # as "processed" once Fenergo receives "GLCMS UID"  through Mule Middleware system
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    When I navigate to 'CaseDetails' task screen
    #Test-data: Validate "Waiting for UID from GLCMS" task is generated
    Then I see "Waiting for UID from GLCMS" task is generated
    #Test-data: Validate "Waiting for UID from GLCMS" task is Assigned to 'KYCMaker: Corporate'
    And I validate "Waiting for UID from GLCMS" task is Assigned to 'KYCMaker: Corporate'
    When I login to Fenergo Application with "KYCMaker: Corporate"
    #Test-data: Validate the status of "Waiting for UID from GLCMS" task is displaying as 'Completed' in task grid
    When I see 'Casedetails' task screen is auto-refreshed and "Waiting for T24 CIF ID from GLCMS" task status is updated as completed
    When I click on 'Navigate' option displaying under options button for "Waiting for T24 CIF ID from GLCMS" task
    When I see "Capture Fab references" task screen is displayed
    Then I validate below labels as below:
      | Fenergo Label Name | Field Type   | Visible | Editable | Mandatory |
      | Legal Entity Name  | Alphanumeric | Yes     | No       | No        |
      | Client Type        | Drop-down    | Yes     | No       | No        |
      | Counterparty UID   | Alphanumeric | Yes     | No       | No        |
    #Test-data: Validate below fields in the grid as per DD(Editable, Visible, Mandatory, Defaults to..) on  "Waiting for UID from GLCMS" task "Capture FAB references" stage
    And I validate below labels in the grid as below:
      | Fenergo Label Name              | Field Type | Visible | Editable | Mandatory |
      | Status                          | Drop-down  | Yes     | No       | No        |
      | GLCMS Status Description        | Drop-down  | Yes     | No       | No        |
      | Request Sent Date and Time      | Date       | Yes     | No       | No        |
      | Response Received Date and Time | Date       | Yes     | No       | No        |
    #Test-data: Validate user is able to navigate to "Waiting for T24 CIF ID from GLCMS" task using 'View Task' button
    When I click on 'View Task' option displying under options button
    Then I see "Waiting for T24 CIF ID from GLCMS" task screen is displayed
    #Test-data: Validate "Status" field is auto-populated as "processed" once Fenergo receives "GLCMS UID" and "GLCMS UID" is
    #auto-populated for "Capture Fab references" task
    And I validate "Status" field is auto-populated with "processed" status under the grid
    #Test-data: Validate Resend button display as disabled for KYC Maker on "Waiting for UID from GLCMS" task when "Status" field is auto-populated
    # as "processed" once Fenergo receives "GLCMS UID"  through Mule Middleware system
    And I validate Resend button display as disabled on 'Capture Fab references' for the request with 'Processed' status
    
