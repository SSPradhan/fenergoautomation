#Test Case: TC_R2EPIC013PBI004_03
#PBI: R2EPIC013PBI004
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC013PBI004_03

	@ToBeAutomated
  Scenario: Verify KYC Maker is able to capture 'GLCMS UID' & 'T24 CIF ID' details in "Capture FAB Reference" task
  #Precondition: Fenergo doesnt receives GLCMS UID and T24 CIF ID and 'Waiting for UID from GLCMS' task is auto completed after reseding for 4 times.
	#Additional scenario: Verify the COB case is closed after completing 'Capture FAB Reference' task manually
	#Additional scenario: Veirfy user is able to view GLCMS UID and T24 CIF in LE360 page
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I add "AssociatedParty" via express addition 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I store the "CaseId" from LE360 
	
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	And I click on "SaveandCompleteforAssessmentScreen1" button 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	#RegularReview
	And I initiate "Regular Review" from action button 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 

	
#	When I navigate to "CloseAssociatedCasesGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
	When I complete "EnrichKYC" screen with key "RegularReviewClientaData" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	And I click on "SaveandCompleteforAssessmentScreen1" button 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Low" 
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	And I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Fenergo doesnt receives GLCMS UID after completing review and approve stage
	#Resend the request for 4 times (Fenergo doesnt receives GLCMS UID and task is autocomplted)
	And I assert that "Waiting for UID from GLCMS" task status is completed
	#Verify 'Capture FAB Reference' tak is trigerred and assigned to KYC Maker
	And I assert that "Capture FAB References" is visible	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I navigate to "Capture FAB Reference" task
	#Fill in GLCMS UID and T24 CIF ID and complete the task
	When I complete "Capture FAB Reference" task 
	#Verify the COB case status is updated to Closed
	And I assert that the CaseStatus is "Closed" 
	#Verify user is able to view GLCMS UID and T24 CIF Id in LE360 page
	When I navigate to LE360 screen	
	And I assert that "GLCMSUID" is available
	And I assert that "T24CIFID" is available
	
	
	
	  