#Test Case: TC_R2EPIC013PBI004_02
#PBI: R2EPIC013PBI004
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC013PBI004_02

	@ToBeAutomated
  Scenario: Verify 'Capture FAB Reference' task is getting trigerred when Fenergo doesnt recieve GLCMS UID after sending request for 4 times
	#Additional scenario: Verify 'Capture FAB Reference' task is assigned to KYC Maker and available in the respective maker team task
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I store the "CaseId" from LE360 
	
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"  
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	#RegularReview
	And I initiate "Regular Review" from action button 
#	Given I login to Fenergo Application with "KYCMaker: Corporate" 
#	When I navigate to "CloseAssociatedCasesGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
	When I complete "EnrichKYC" screen with key "RegularReviewClientaData" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	And I click on "SaveandCompleteforAssessmentScreen1" button 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Low" 
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	And I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Fenergo doesnt receives GLCMS UID after completing review and approve stage
	When I navigate to "Waiting for UID from GLCMS" task
	#Resend the GLCMS UID request for 1st time
	And I click on "Resend" button
	#Fenergo doesnt receives GLCMS UID after sending request for 1st time
	#Verify Capture FAB Reference task is not triggered
	And I assert that "Capture FAB Reference" task is not available
	#Resend the GLCMS UID request for 2nd time
	When I navigate to "Waiting for UID from GLCMS" task
	And I click on "Resend" button
	#Fenergo doesnt receives GLCMS UID after sending request for 2nd time
	#Verify Capture FAB Reference task is not triggered
	And I assert that "Capture FAB Reference" task is not available
	#Resend the GLCMS UID request for 3rd time
	When I navigate to "Waiting for UID from GLCMS" task
	And I click on "Resend" button
	#Fenergo doesnt receives GLCMS UID after sending request for 3rd time
	#Verify Capture FAB Reference task is not triggered
	And I assert that "Capture FAB Reference" task is not available
	#Resend the GLCMS UID request for 4th time
	When I navigate to "Waiting for UID from GLCMS" task
	And I click on "Resend" button
	#Fenergo doesnt receives GLCMS UID after sending request for 4th time
	#Verify 'Waiting for UID from GLCMS' task is autoccompleted (Status should be updated as 'completed')
	And I assert that "Waiting for UID from GLCMS" task status is completed
	#Verify Capture FAB Reference task is getting triggered
	And I assert that "Capture FAB References" is visible	
	#Verify 'Capture FAB Reference' task is assiged to KYC Maker and available in respective maker team task
	#Verify KYC Maker is able to view 'Navigate' and 'View' option in Capture FAB References task (three dot (...))
	#Verify KYC Maker able to navigate to Capture FAB References task by clicking on three (...) and clicking on Navigate option
	#Verify KYC Maker able to View Capture FAB References task by clicking on three (...) and clicking on View option
	
	


	
  