#Test Case: TC_WAC_Corporate_14
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_14

  Scenario: Validate adding multiple layer (3 layers) associated parites (Individual / Non-Individual). Adding Individual AP with UBO relationship (Very High score) in the third layer. 
    #Refer to TC14 in the WAC Corp Data Sheet