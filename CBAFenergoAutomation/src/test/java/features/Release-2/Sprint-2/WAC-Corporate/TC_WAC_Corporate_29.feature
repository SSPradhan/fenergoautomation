#Test Case: TC_WAC_Corporate_29
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Priyanka
#Last Edited by: Priyanka
Feature: TC_WAC_Corporate_29

  Scenario: Derive Overall Risk Rating as'Medium-Low' by filling details in all mandatory fields and one non-mandatory field (Select value Industry (Primary) that are considered for Risk
    #Refer to TC37 in the WAC Corp Data Sheet