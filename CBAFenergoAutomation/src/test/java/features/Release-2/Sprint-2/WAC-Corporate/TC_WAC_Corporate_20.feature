#Test Case: TC_WAC_Corporate_20
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_20

  Scenario: RR-Derive the overrall risk rating as High (COB overall risk rating is Medium)
    #Refer to TC20 in the WAC Corp Data Sheet