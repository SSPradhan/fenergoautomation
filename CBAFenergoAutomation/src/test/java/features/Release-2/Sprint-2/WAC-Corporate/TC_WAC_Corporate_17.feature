#Test Case: TC_WAC_Corporate_17
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_17

  Scenario: COB-Refer back and derive the overrall risk rating as Medium-Low (previous risk rating High)
    #Refer to TC17 in the WAC Corp Data Sheet