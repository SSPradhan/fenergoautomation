#Test Case: TC_R2EPIC014PBI004_05
#PBI: R2EPIC014PBI004
#User Story ID:Corp/PCG-9, FIG-9
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Escalation Process

  Scenario: Client type:NBFI-Verify the new fields 'Email Chaser' and 'Escalation Action' are available in the screen after completing the Task details in 'Capture Risk Category' screen for RR case
    #Precondition: 'Capture Risk Category' task Status = 'Pause awaiting client response' and Email Chaser = 'Notification to RM/BUH/BH'
    #Additional Scenario: Change the task status to 'In progress' and proceed till Review and approval stage
    #Additional Scenario: Refer back from Review and approval stage to Risk Asessment stage and check new fields 'Email Chaser' and 'Escalation Action' are NOT available
    #Additional Scenario: After referring back change the status to 'Account Blocked' and check not able to complete the task 'Capture Risk Category' task
    #Additional Scenario: Verify user is able to complete the 'Capture Risk Category' task when status changed to 'In-Progress'
    #Additional Scenario: Verify the case status is changed to 'Completed' when completing the task status with In-Progress
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "FIG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I add "AssociatedParty" via express addition
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert the case is closed
    #Test-data: Initiate regular Review Workflow
    When I navigate to 'LE360' screen
    When I Click on 'Actions' button and select 'RegularReview' workflow
    Then I see 'RegularReview' Workflow has been triggered
    And I navigated to 'CloseAssociatedCases' task
    Then I complete 'CloseAssociatedCases' task
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I complete "ValidateKYCandRegulatoryGrid" task
    When I navigate to "ReviewrequestDetails" task
    Then I complete "ReviewrequestDetails" task
    #Complete "Review/edit client data" task
    When I navigate to "Review/edit client data" task
    Then I Complete "Review/edit client data" task
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    # Verify 'Complete AML' task has been triggered
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key
    And I click on "SaveandCompleteforAssessmentScreen1" button
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Googlescreening" by rightclicking
    And I complete "Googlescreening" from assessment grid with Key
    Then I complete "CompleteAML" task
    # Verify 'Complete ID&V' task has been triggered
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Select the Risk category as "Medium-low" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'medium-low' risk rating
    #Navigate to Task Details screen
    When I click on 'Edit task' button displaying on the options button on 'Review/editclientdata' task
    When I navigate to 'taskdetails' screen
    #Precondition:Capture Risk Category' task Status = 'Pause awaiting client response' and Email Chaser = 'Notification to RM/BUH/BH'
    #Verify the new fields 'Status' is available in the screen
    Then I Verify the new fields 'Status' is available in the screen
    #Test-data: Verify the new fields 'Email Chaser' and 'Escalation Action' are available in the screen
    When I select the value of Status drop-down as 'Pause awaiting client response'
    Then I see 'Email Chaser' Field is visible on the screen
    When I enter value in Email Chaser as 'Notification to RM/BUH/BH'
   #Additional Scenario:Change the task status to 'In progress' and proceed till Review and approval stage
    When I Select the value of Status drop-down as 'In-Progress'
    When I save the details on 'taskdetails' task
    #Complete "Review/edit client data" task
    When I navigate to "Review/edit client data" task
    Then I Complete "Review/edit client data" task
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    # Verify 'Complete AML' task has been triggered
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key
    And I click on "SaveandCompleteforAssessmentScreen1" button
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Googlescreening" by rightclicking
    And I complete "Googlescreening" from assessment grid with Key
    Then I complete "CompleteAML" task
    # Verify 'Complete ID&V' task has been triggered
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Select the Risk category as "Medium-low" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'medium-low' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Additional Scenario: Refer back from Review and approval stage to Risk Asessment stage and check new fields 'Email Chaser' and 'Escalation Action' are NOT available
    #Refer case to 'Risk Assessment' stage
    When I click on 'Actions' button and select 'refer' option
    When I select refer to Stage as 'RiskAssessment' stage
    #Verify case is referred to 'RiskAssessment' stage
    Then I see case has been referred to 'RiskAssessment' stage
    #Navigate to Task Details screen
    When I click on 'Edit task' button displaying on the options button on 'RiskAssessment' task
    When I navigate to 'taskdetails' screen
    # Verify'Email Chaser' and 'Escalation Action' are NOT available
    Then I verify 'Email Chaser' and 'Escalation Action' are NOT available
    When I navigate to 'Casedetails' task
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task    
    #Additional Scenario: After referring back change the status to 'Account Blocked' and check not able to complete the task 'Capture Risk Category' task
    #Refer case to 'Risk Assessment' stage
    When I click on 'Actions' button and select 'refer' option
    When I select refer to Stage as 'RiskAssessment' stage
    #Verify case is referred to 'RiskAssessment' stage
    Then I see case has been referred to 'RiskAssessment' stage
    #Navigate to Task Details screen
    When I click on 'Edit task' button displaying on the options button on 'RiskAssessment' task
    When I navigate to 'taskdetails' screen
    #select the value of Status drop-down as 'Account Blocked'
    When I select the value of Status drop-down as 'Account Blocked'
    When I save the details by clicking on Save button
    When I navigate to "CaptureRiskCategoryGrid" task
    # Verify user is not able to complete the task 'Capture Risk Category' task(Save and complete button display as disabled)
    Then I verify user is not able to complete the task 'Capture Risk Category' task
    And Task status is displaying as 'Account Blocked'
     #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button  
   #Additional Scenario: Verify user is able to complete the 'Capture Risk Category' task when status changed to 'In-Progress' 
    #Refer case to 'Risk Assessment' stage
    When I click on 'Actions' button and select 'refer' option
    When I select refer to Stage as 'RiskAssessment' stage
    #Verify case is referred to 'RiskAssessment' stage
    Then I see case has been referred to 'RiskAssessment' stage
    #Navigate to Task Details screen
    When I click on 'Edit task' button displaying on the options button on 'RiskAssessment' task
    When I navigate to 'taskdetails' screen
    #select the value of Status drop-down as 'Account Blocked'
    When I select the value of Status drop-down as 'In-progress'
    When I save the details by clicking on Save button
    When I navigate to "CaptureRiskCategoryGrid" task
    # Verify user is able to complete the task 'Capture Risk Category' task(Save and complete button display as Enabled)
    Then I verify user is able to complete the task 'Capture Risk Category' task
    And Task status is displaying as 'In-Progress'
    #Additional Scenario: Verify the case status is changed to 'Completed' when completing the task status with In-Progress
    When I complete "CaptureRiskCategoryGrid" task
    Then I Validate Task status is displaying as 'Completed'
    
    