#Test Case: TC_R2EPIC014PBI003_07
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_07

  @Automation
  Scenario: FI:Validate the field behaviours in 'Source Of Funds And Wealth Details' and 'Industry Codes Details' section of Review/Edit Client Data Screen in RR workflow
    #Precondition: COB case with Client type as FI and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
     #=Initiate Regular Review
     And I initiate "Regular Review" from action button 
		#=Then I navigate to "CloseAssociatedCasesGrid" task 
    When I complete "CloseAssociatedCase" task 
	Then I store the "CaseId" from LE360 
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "Review/EditClientDataTask" task
    And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      | Label                                  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Legal Entity Source of Income & Wealth | NA        | false   | NA       | NA        | NA         |
      | Legal Entity Source of Funds           | NA        | false   | NA       | NA        | NA         |
    And I validate the following fields in "Industry Codes Details" Sub Flow
      | Label                                 | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Primary Industry of Operation Islamic | Dropdown  | true    | true     | false     | NA         |
      | Primary Industry of Operation         | Dropdown  | true    | false    | true      | NA         |
      | Primary Industry of Operation UAE     | Dropdown  | true    | false    | true      | NA         |
    And I validate the following fields in "Industry Code Details" Sub Flow
      | Label                           | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo              |
      | Secondary Industry of Operation | MultiSelectDropdown | true    | false    | false     | NA |
    And I check that below data is not visible
      | FieldLabel          |
      | NAIC                |
      | ISIN                |
      | Secondary ISIC      |
      | NACE 2 Code         |
      | Stock Exchange Code |
      | Central Index Key   |
      | SWIFT BIC           |
    And I verify "Primary Industry of Operation" drop-down values
    And I verify "Primary Industry of Operation UAE" drop-down values
    And I verify "Secondary Industry of Operation" drop-down values
    
    
    
  
  
  
  
  
  
    #=> Below steps are written for Manual Testing
    #Given I login to Fenergo Application with "KYCMaker_FIG"
    #When I search for the "CaseId"
    #Initiate Regular Review
    #When I select "RegularReview" from Actions menu
    #When I navigate to "CloseAssociatedCases" task
    #And I click on "SaveandComplete" button
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "ReviewEditClientData" task
    #Source Of Funds And Wealth Details section
    #Validate in Source Of Funds And Wealth Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the FieldType, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      #| FieldLabel                             | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      #| Legal Entity Source of Income & Wealth |           | false   |          |           |                   |
      #| Legal Entity Source of Funds           |           | false   |          |           |                   |
    ##
    #Industry Code details section
    #Validate in Industry Codes Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the FieldType, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      #| FieldLabel                            | FieldType | Visible | Editable | Mandatory | Field Defaults To       |
      #| Primary Industry of Operation Islamic | Dropdown  | true    | false    | true      | Auto populated from COB |
      #| Primary Industry of Operation         | Dropdown  | true    | true     | true      | Auto populated from COB |
      #| Primary Industry of Operation UAE     | Dropdown  | true    | true     | false     | Auto populated from COB |
    ##
    #Validate the below new fields are available (new fields) in Industry Code Details section and values are autopopulated from COB case
    #Validate the FieldType, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Industry Code Details" Sub Flow
      #| FieldLabel                      | FieldType | Visible | Editable | Mandatory | Field Defaults To       |
      #| Secondary Industry of Operation | Dropdown  | true    | true     | false     | Auto populated from COB |
    #Validate the below fields are hidden in Industry Code Details section
    #And I validate the following fields in "Industry Code Details" Sub Flow
      #| Label               | Visible |
      #| NAIC                | false   |
      #| ISIN                | false   |
      #| Secondary ISIC      | false   |
      #| NACE 2 Code         | false   |
      #| Stock Exchange Code | false   |
      #| Central Index Key   | false   |
      #| SWIFT BIC           | false   |
    ##
    #LOV validation - Primary Industry of Operation. (Refer PBI-LOV tab)
    #And I validate the LOV of "PrimaryIndustryofOperation" with key "PrimaryIndustryofOperation"
    ##
    #LOV validation - Primary Industry of Operation UAE. (Refer PBI-LOV tab)
    #And I validate the LOV of "PrimaryIndustryofOperationUAE" with key "PrimaryIndustryofOperationUAE"
    ##
    #LOV validation - Secondary Industry of Operation. (Refer PBI-LOV tab)
    #And I validate the LOV of "SecondaryIndustryofOperation" with key "SecondaryIndustryofOperation"
