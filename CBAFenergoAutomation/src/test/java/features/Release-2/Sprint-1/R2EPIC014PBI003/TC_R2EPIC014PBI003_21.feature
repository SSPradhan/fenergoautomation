#Test Case: TC_R2EPIC014PBI003_21
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_21

  @To_be_automated
  Scenario: Validate the ability to complete the 'Review Client data' stage by changing all the field values, sub flows, documents which are defaulted from COB case
    #Precondition: COB case with any Client type to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Change all the field values which are defaulted from the COB case in all the sections
    #Edit sub flows for Products, Addresses
    #Add more sub flows for Products, Addresses
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Change all the field values which are defaulted from the COB case in all the sections
    #Edit sub flows for Products, Addresses, Relationships etc..
    #Add more sub flows for Products, Addresses, Relationships, Contacts, Tax Identifer etc..
    When I complete "ReviewEditClientData" screen with key "C1"
    When I navigate to "KYCDocumentRequirementsGrid" task
    #Modify the existing documents
    #Remove the existing document and add new document
    #Add non-mandatory documents
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    #Verify able to complete the stage successfully
    #Refer back to Review Client Data Stage
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Verify all the data are retained in this screen
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Verify all the data are retained in this screen
    When I complete "ReviewEditClientData" screen with key "C1"
    When I navigate to "KYCDocumentRequirementsGrid" task
    #Verify all the documents are retained in this screen
    ##
    