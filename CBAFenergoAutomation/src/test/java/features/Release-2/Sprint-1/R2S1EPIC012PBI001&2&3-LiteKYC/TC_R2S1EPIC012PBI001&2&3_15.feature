#BBG_Test Case: TC_R2S1EPIC012PBI001&2&3_15
#PBI: R2S1EPIC012PBI001, R2S1EPIC012PBI002, R2S1EPIC012PBI003
#User Story ID: Light_KYC_001, Light_KYC_006, Light_KYC_015
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Lite KYC

  Scenario: Validate if Lite KYC is not triggered when "Does the CDD profile qualify for Lite KYC?" field is selected as "Yes" for "FI" client type and LE Category "Hedge Funds" and Legal Entity Role is not chosen as "Client/Counterparty" 
    Given I login to Fenergo Application with "RM:FI"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - FI
    When I complete "Enter Entity details" screen task with ClientEntityType as "FI"
    When I complete "Search For Duplicates" screen task
    Then I select "Hedge Funds" for Legal Entity Category field
    And I select "Broker" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    And I assert "CREATE ENTITY" button is not enabled
    And I select "Yes" for "Does the CDD profile qualify for Lite KYC?" field
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered


  Scenario: Validate if Lite KYC is not triggered when "Does the CDD profile qualify for Lite KYC?" field is selected as "Yes" for "NBFI" client type and LE Category "Brokers & Securities Companies" and Legal Entity Role is not chosen as "Client/Counterparty" 
    Given I login to Fenergo Application with "RM:FI"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - FI
    When I complete "Enter Entity details" screen task with ClientEntityType as "FI"
    When I complete "Search For Duplicates" screen task
    Then I select "Brokers & Securities Companies" for Legal Entity Category field
    And I select "Prime Broker" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    And I assert "CREATE ENTITY" button is not enabled
    And I select "Yes" for "Does the CDD profile qualify for Lite KYC?" field
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered