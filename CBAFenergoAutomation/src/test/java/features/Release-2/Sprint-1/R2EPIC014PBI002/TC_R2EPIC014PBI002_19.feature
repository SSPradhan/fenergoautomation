#Test Case: TC_R2EPIC014PBI002_19
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-8, FIG-8
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Regular Review

  Scenario: Validate the DB length validation for the following editable Alphanumeric fields in "Review Request" task for RM and KYC Maker
    #Validate LOVs for editable dropdown fields
    #Precondition: Closed COB case
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    When I navigate to "Review Request Details" task as "KYC Maker"
    #Validate if the following fields are accepting more than the length of characters mentioned against the repective fields
    And I assert that following fields are accepting more than the length of characters mentioned against the repective fields
      | Purpose of Account/Relationship                            | 1000 |
      | Justification for opening/maintaining non-resident account | 1000 |
      | Client Reference                                           |  128 |
    Then I assign the task to RM
    #The above action can be performed by clicking the Actions(...) button against the task and choosing "Edit task"
    And I assert that "Review Request Details" task is assigned to "RM" team
    And I navigate to "Review Request Details" task as "RM"
    #Validate if the following fields are accepting more than the length of characters mentioned against the repective fields
    And I assert that following fields are accepting more than the length of characters mentioned against the repective fields
      | Purpose of Account/Relationship                            | 1000 |
      | Justification for opening/maintaining non-resident account | 1000 |
      | Client Reference                                           |  128 |
    #Validate LOVs for editable dropdown fields
    And I validate LOVs for the following fields againt PBI
      | Country of Incorporation / Establishment |
      | Country of Domicile/ Physical Presence   |
      | Channel & Interface                      |
      | Residential Status                       |
      | Customer Tier                            |
      | Relationship with bank                   |
      | Emirate                                  |
      | Booking Country                          |
      | Consented to Data Sharing Jurisdictions  |
      | Confidential                             |
      | Target Code                              |
      | Sector Description                       |
      | UID Originating Branch                   |
