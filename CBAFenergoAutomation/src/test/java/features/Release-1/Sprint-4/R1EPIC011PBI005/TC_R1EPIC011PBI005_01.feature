#Test Case: TC_R1EPIC011PBI005_01
#PBI: R1EPIC011PBI005
#User Story ID: US137, US140, US068 (SECONDARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar
Feature: TC_R1EPIC011PBI005_01 

@Automation 
Scenario: 
	Validate behaviour of below fields in KYC condition section & Regulatory data section of Validate KYC and Regulatory Data screen 
	#Is the Entity operating with Flexi Desk?
	#Legal Entity Category
	#Countries of Business Operations/Economic Activity
	#Updated(08-04-21) : Legal Entity Category field is not present anymore in UI
	Given I login to Fenergo Application with "RM:BBG" 
	#=Create entity with Country of Incorporation = UAE and client type = Business Banking Group
	When I create a new request with FABEntityType as "Business Banking Group (BBG)" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "BBG" and below data 
		|Product|Relationship|
		|C1			|C1				   |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	#Verify the below behaviour in KYC condition section
	#Validate the Legal entity category value is defaulted from complete screen
	And I validate the following fields in "Customer Details" Sub Flow 
      | Label                                              | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo   | 
      | Is the Entity operating with Flexi Desk?           | Dropdown            | true    | false    | true      | Select...    | 
      | Legal Entity Category                              | NA			             | false   | NA       | NA        | NA					 | 
      | Countries of Business Operations/Economic Activity | MultiSelectDropdown | true    | false    | true      | Select...    | 
	And I take a screenshot 
	And I verify "Is the Entity operating with Flexi Desk?" drop-down values 
#	And I verify "Legal Entity Category" drop-down values with ClientType as "Business Banking Group (BBG)" 
	
	