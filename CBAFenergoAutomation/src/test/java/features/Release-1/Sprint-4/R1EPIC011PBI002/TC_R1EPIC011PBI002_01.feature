#Test Case: TC_R1EPIC011PBI002_01
#PBI: R1EPIC011PBI002
#User Story ID: US112, US114, US100
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI002_01

  @Automation
  Scenario: Validate behaviour of below fields in Search For Duplicates screen
    #Client Type
    #Legal Entity Type
    #Country of Incorporation / Establishment
    Given I login to Fenergo Application with "RM:BBG"
    #Creating a legal entity with "Client Entity Type" as "Business Banking Group" and "Legal Entity Role" as "Client/Counterparty"
    #Create entity by selecting New request and enter data and click on Search button
    When I complete "Duplicate" task 
   And I validate the following fields in "EnterEntityDetails" Sub Flow 
      | Label                                    | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo     | 
      | Client Type                              | Dropdown  | true    | false    | true      | Select...      | 
      | Legal Entity Type                        | Dropdown  | true    | false    | true      | Select...      | 
      | Entity Type                              | Dropdown  | true    | false    | false     | Non Individual | 
      | Country of Incorporation / Establishment | Dropdown  | true    | false    | true      | Select...      | 
    #Verify newly added lov "Business Banking Group" is available in the field 'Client Type'
   And I verify "Client Type" drop-down values
    #Verify the 38 lovs are dispalying in Legal Entity Type field when client type is selected as Business Banking Group
#    And I validate "Legal Entity Type" drop-down has following LOVs
#    #List from LOV All tab from the PBI R1EPIC011PBI001
#    And I validate "Country of Incorporation / Establishment" drop-down has following LOVs
#		#Refer PBI (countries)	
