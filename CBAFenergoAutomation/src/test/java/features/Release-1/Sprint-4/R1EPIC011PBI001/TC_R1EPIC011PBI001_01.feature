#Test Case: TC_R1EPIC011PBI001_01
#PBI: R1EPIC011PBI001
#User Story ID: US112, US114, US100
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI001_01 

@Automation 
Scenario: Validate behaviour of below fields in Enter Entity Details screen 
#Client Type
#Legal Entity Type
#Country of Incorporation / Establishment
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	#Creating a legal entity with "Client Entity Type" as "Business Banking Group" and "Legal Entity Role" as "Client/Counterparty"
	When I complete "CompleteRequestFAB" in "CaptureRequestDetails" screen 
	And I select "Corporate" for "Dropdown" field "Client Type" 
	And I validate the following fields in "EnterEntityDetails" Sub Flow 
		| Label             | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Client Type       | Dropdown  | true    | false     | true      | Select...  |
		| Legal Entity Type | Dropdown  | true    | false     | true      | Select...  |
		| Entity Type       | Dropdown  | true    | false    | false     | Non Individual |
		| Country of Incorporation / Establishment | Dropdown  | true    | false   | true      |  Select...          |
	And I verify "Country of Incorporation / Establishment" drop-down values 
	And I clear "Client Type" field 
	#	And I check that below data is not visible 
	#		|FieldLabel|
	#		|Country of Incorporation / Establishment|
	And I take a screenshot 
	And I verify "Client Type" drop-down values 
	