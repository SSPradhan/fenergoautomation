#Test Case: TC_R1S3EPIC007PBI001.3_01
#PBI: R1S3EPIC007PBI001.3
#User Story ID: D1.6
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Update Documents

  @Tobeautomated
  Scenario: Validate "Onboarding Maker" is able to update the document from document grid for the following task screens
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I click on plus sign displyaing at the top of "Documents" section
    When I add the document and "save"
    When I click on "documentdetails" from options button displaying corresponding to attached document in the grid
    When I navigate to "DocumentDetails" screen
    #Test-data: Validate Update Document feature 
    When I add document by browsing  and "save" the details
    Then I can see document is updated under update document grid

 	  #Repeat the same validation for all the remaining 7 screens
    #New Request>Capture Request Details> Document Details
		#Enrich KYC Info> KYC Document Requirements > Document Details
		#AML>Complete ID&V>Edit Verification>Document Details
		#AML > Complete AML > Document Details
		#Capture Request Details > Product > Document Details
		#LE360> Documents
		#Enrich KYC Profile > Tax Identifier > Document Details
		#AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
 	