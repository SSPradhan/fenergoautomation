#Test Case: TC_R1S3EPIC007PBI001.1_04
#PBI: R1S3EPIC007PBI001.1
#User Story ID: D1.3, D1.4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Retrieve Documents and Retrieve all document versions

  @Tobeautomated
  Scenario: Validate "RM User" is able to Retrieve document on document details screen for "Capture Request Details" task
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    When I add document by browsing from Local folder and "save" the details
    Then I can see document is added under document requirement
    #Test-data: Validate user is able to retrieve document on document details screen
    When I click on "ViewDocument" button corresponding to the added document
    Then I can see document is downloaded and user is able to view the document

  Scenario: Validate "RM User" is able to add document (by uploading)from Shared folder/Sharepoint on document details screen for "Capture Request Details" task
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I navigate to "DocumentDetails" screen
    When I add document by browsing and "save" the details
    Then I can see document is added under document requirement
    When I navigate to "Documentdetails" task by clicking on Document details screen again
    When I add other document (by overriding pervious doc) and save the details
    When I Navigate to Document details screen by clicking on Document details screen again
    #Test-data: Validate user is able to retrieve all document on document details screen
    Then I can see all previously added documents can be seen under "PreviousVersions" section
    When I click on "View" button corresponding to any version
    Then I can see document is downloaded and user is able to view the document
 	#Repeat the same validation for all the remaining 7 screens
    #New Request>Capture Request Details> Document Details
		#Enrich KYC Info> KYC Document Requirements > Document Details
		#AML>Complete ID&V>Edit Verification>Document Details
		#AML > Complete AML > Document Details
		#Capture Request Details > Product > Document Details
		#Enrich KYC Profile > Tax Identifier > Document Details
		#AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
