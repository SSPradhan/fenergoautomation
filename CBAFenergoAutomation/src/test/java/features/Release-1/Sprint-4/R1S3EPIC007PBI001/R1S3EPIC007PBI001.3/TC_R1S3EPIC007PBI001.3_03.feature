#Test Case: TC_R1S3EPIC007PBI001.3_03
#PBI: R1S3EPIC007PBI001.3
#User Story ID: D1.13
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Audit Trail

  @Tobeautomated
  Scenario: Validate "Onboarding Maker" is able to view the  Audit trail for documents added, removed and modified under History section of LE details screen
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I click on plus sign displaying at the top of "Documents" section
    When I add the document and "save"
    When I click on plus sign displaying at the top of "Documents" section
    When I add the document and "save"
    When I click on "documentdetails" from options button displaying corresponding to attached document in the grid
    When I navigate to "DocumentDetails" screen
    When I click on "RemoveDocument" button
    Then I see Document is removed from the document grid
    When I navigate to History section on "LEdetails" task
    #Test-data: Validate Audit trail feature under History section on LEdetails screen
    Then I can see history of Added/removed/Updated document under history section
