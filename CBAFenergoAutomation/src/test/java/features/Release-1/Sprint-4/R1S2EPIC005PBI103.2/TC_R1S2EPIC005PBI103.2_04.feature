#Test Case: TC_R1S2EPIC005PBI103.2_04
#PBI: R1S2EPIC005PBI103.2
#User Story ID: FIG10, FIG11, FIG12, FIG13, FIG14, FIG15
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103.2_04

  @Automation
  Scenario: Verify New added Fields and Grids added on "Edit Verification" task of "Complete ID&V" task for "AML"  stage
    
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "Non FI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button

    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task

    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    	#When I Right click on hologram to Add Associated Party and add "non-Individual" type Association and save it
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    	#Then I can see already added association under "Associatedparties" section
    	#When I click on "Edit" button displaying corresponding to the added association
    	#When I navigate to "EditVerification" task
    When I complete "EditID&V" task	
    	#Then I can see below mentioned Newly added Fields and grids on LE details on "EditVerification" task
    	#And I verify below fields under "LE details" section
    And I check that below data is visible
 	    |FieldLabel																|
      | Legal Entity Name                       |
      | Client Type                             |
      | Legal Entity Type                       |
      | Country of Incorporation / Establishment |
      | Country of Domicile / Physical Presence   |
      | LEI                                     |
      | Name of Registration Body               |
    	#And I verify below fields under	"Entity Under Verification" section
    And I click on "EntityUnderVerification" button
    And I check that below data is visible	
    	|FieldLabel			|
      | ID&V Category |
    And I check that below subflow is visible
  	  | Subflow				 |
      | Addresses      |
      | Documents      |
      | Tax Identifier |

