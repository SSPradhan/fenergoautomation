#Test Case: TC_R1S2EPIC006PBI001_01
#PBI: R1S2EPIC006PBI001
#User Story ID: US26
#Designed by: Anusha PS
#Last Edited by: Anusha PS
@Automation
Feature: TC_R1S2EPIC006PBI001_01-Screening

  Scenario: Validate if "Screening Decision" panel is not visible in Complete AML task - Assessment - Fircosoft Screening	and validate if "Onboarding Maker" is able to add documents and comments
    
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    #And I assert "Assessment Decision" section is not visible
    #And I assert "Confirmed Matches" section is the last section in the screen
    And I check that below data is not visible
      | FieldLabel          | 
      | Assessment Decision | 
    And I check that below subflow is visible
      | Subflow           | 
      | Confirmed Matches |
    And I take a screenshot
    When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
#		And I click on "SaveandCompleteforAssessmentScreen1" button
		#And I assert "Assessment Decision" section is not visible
    #And I assert "Confirmed Matches" section is the last section in the screen
		And I check that below data is not visible
      | FieldLabel          | 
      | Assessment Decision | 
    And I check that below subflow is visible
      | Subflow           | 
      | Confirmed Matches |
    And I take a screenshot  
    
    
