#Test Case: TC_R1S2EPIC006PBI003_02
#PBI: R1S2EPIC006PBI003
#User Story ID: 19
#Designed by: Anusha PS
#Last Edited by: Anusha PS
@To_be_automated
Feature: Screening

  Scenario: Validate metadata and LOVs for the fields under "PEP" in Complete AML task - Assessment - Google Screening
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    #Add Google Screening by right clicking the legal entity from hierarchy and clicking "Add Google Screening"
    And I add Google Screening for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    And I navigate to "Assessment" screen
    #To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
    And I navigate to "Google Screening" screen
    #Validation for PEP
    And I validate "PEP" is the first section under "Google Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the only following field in "PEP" section
      | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | PEP Status         | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "PEP Status" field
      | No Match       |
      | False Match    |
      | Positive Match |
    And I validate 'PEP Category' is not visible
    And I select "Positive Match" for "PEP Status" field
    And I validate 'PEP Category' is visible
    And I validate the conditional field in "PEP" section
      | Fenergo Label Name | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | PEP Category       | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "PEP Category" field
    #Refer PBI for LOV list
    And I select "False Match" for "PEP Status" field
    And I validate 'PEP Category' is not visible
   