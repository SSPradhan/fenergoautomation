#Test Case: TC_R1S2EPIC002PBI038_02
#PBI: R1S2EPIC002PBI038
#User Story ID: US068
#Designed by: Niyaz Ahmed (as part of R1EPIC002PBI008)
#Last Edited by: Anusha PS
Feature: TC_R1S2EPIC002PBI038_02-Capture New Request Details - Complete Legal Entity Category

  @Automation @TC_R1S2EPIC002PBI038_02
  Scenario: 
    Validate the 'Legal Entity Category' dropdown is filtered with relevant values (13) in Complete screen and other secondary screens when 'Client Type' is selected as 'PCG-Entity' from the Enter entity details screen (Refer lov in the PBI)

    Given I login to Fenergo Application with "RM:PCG"
    #Select client Type as 'PCG-Entity' in Enter entity details screen
    #When I create a new request with FABEntityType as "PCG-Entity" and LegalEntityRole as "Client/Counterparty"
    #Validate the Legal Entity Category lovs (Refer lov from the PBI) in complete screen
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "PCG-Entity"
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
    #=Regression Comments on 04-04-21:Legal Entity Category is now no longer present in the screen hence commented
    #	Then I verify "Legal Entity Category" drop-down values with ClientType as "PCG-Entity"
    #Refer PBI for LOVs
    #When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    When I complete "NewRequest" screen with key "PCG"
    #Refer PBI for LOVs
    And I complete "CaptureNewRequest" with Key "PCG-Entity" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    And I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
    #	Then I verify "Legal Entity Category" drop-down values with ClientType as "PCG-Entity"
    #Refer PBI for LOVs
    And I complete "ValidateKYC" screen with key "PCG-Entity"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    #And I check that "Legal Entity Category" is readonly
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
	#    And I Validate the Legal entity category value is defaulted from complete screen/Validate KYC and Regulatory Data
