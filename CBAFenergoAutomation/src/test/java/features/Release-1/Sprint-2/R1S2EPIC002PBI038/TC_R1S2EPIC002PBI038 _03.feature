#Test Case: TC_R1S2EPIC002PBI038_03
#PBI: R1S2EPIC002PBI038
#User Story ID: US068
#Designed by: Niyaz Ahmed (as part of R1EPIC002PBI008)
#Last Edited by: Vibhav Kumar (Regression run 05-04)
Feature: TC_R1S2EPIC002PBI038_03-Capture New Request Details - Complete Legal Entity Category

  @Automation @TC_R1S2EPIC002PBI038_03
  Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (15) in Complete screen and other secondary screens when 'Client Type' is selected as 'FI' from the Enter entity details screen (Refer lov in the PBI)
    Given I login to Fenergo Application with "RM:FI"
    #Select client Type as 'FI' in Enter entity details screen
    #    When I create a new request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    #Validate the Legal Entity Category lovs (Refer lov from the PBI) in complete screen
    #    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
    When I complete "NewRequest" screen with key "FI"
    #    Then I verify "Legal Entity Category" drop-down values with ClientType as "FI"
    #Refer PBI for LOVss
    #=Regression Comments on 04-04-21:Legal Entity Category is now no longer present in the screen hence commented
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Then I verify "Legal Entity Category" drop-down values with ClientType as "Financial Institution (FI)"
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
    #Refer PBI for LOVs
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    #And I check that "LegalEntityCategory" is readonly
    Then I check that below data is not visible
      | FieldLabel            |
      | Legal Entity Category |
