#Test Case: TC_R1S2EPIC002PBI038_06
#PBI: R1S2EPIC002PBI038
#User Story ID: OOTBF001a
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Capture New Request Details - Complete Legal Entity Category

  Scenario: Validate the LOV "Prospect" is removed from "Legal Entity Role" dropdown in all secondary screens  
	Given I login to Fenergo Application with "RM" 
	When I navigate to "Advanced Search" screen
	When I expand the "Filters" button
	Then I verify "Prospect" LOV is not present in "Legal Entity Role" dropdown
	When I navigate to "Legal Entity Search" screen
	When I expand the "Advanced Search" button
	Then I verify "Prospect" LOV is not present in "Legal Entity Role" dropdown
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureRequestDetailsFAB" task
		When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYCandRegulatoryFAB" task 
    When I navigate to "EnrichKYCProfileGrid" task
    When I navigate to "Associated Parties" subflow
    When I expand the "Filters" button
    Then I verify "Prospect" LOV is not present in "Legal Entity Role" dropdown
	