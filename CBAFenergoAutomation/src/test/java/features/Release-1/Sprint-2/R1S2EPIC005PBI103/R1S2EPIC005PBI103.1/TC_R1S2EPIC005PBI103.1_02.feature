#Test Case: TC_R1S2EPIC005PBI103.1_02
#PBI: R1S2EPIC005PBI103.1
#User Story ID: FIG0, FIG1, FIG4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103.1_02

  @Automation
  Scenario: Verify the document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag on
    "document details screen" for "KYC document Requirements" task :

    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate" 
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    # Test-Data:Verify document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag
    And I verify below Documents are Mandatory
      | Documents                                          |
      | Proof of Address (Client)                          |
      | Identification & Verification of Ownership Docu... |
      | Articles of Association (AOA)                      |
      | Memorandum of Association (MOA)                    |
      | Certificate of Incorporation or Trade License o... |
      | KYC Form                                           |
