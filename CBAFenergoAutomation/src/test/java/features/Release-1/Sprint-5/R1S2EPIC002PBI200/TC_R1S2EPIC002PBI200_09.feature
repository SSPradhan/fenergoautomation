#Test Case: TC_R1S2EPIC002PBI200_09
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_09 


@Automation
Scenario: 
	Validate new fields on "LE details" screen for the Associated LE (for "client" relationship) added via Express addition on "Capture Hierarchy details" screen 
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I fill the data for "CaptureNewRequest" with key "C1" 
	And I click on "Continue" button 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnBoardingMaker" 
	When I search for "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I Complete "EnrichKYCProfileGrid" task 
	When I navigate to "CaptureHierarchydetails" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	And I navigate to "LEDetails" screen of added AssociatedParty 
	#Testdata: Verify below mentioned New fields on "LE details" screen
	And I validate the following fields in "LE Details" Sub Flow 
		| Label         				             | FieldType   	| Visible | ReadOnly | Mandatory |
		| Trade Licence Number 						 | Alphanumeric   | true   |false     | NA     |
		| Trading Licence Place of Issue  			 | Alphanumeric   | true    | false     | NA     |
		| Country of Incorporation /Establishment	 | Dropdown   	  | true    | false    | true      |
		| Group name   				                 | Alphanumeric   | true    | false    |false     |
		
		
