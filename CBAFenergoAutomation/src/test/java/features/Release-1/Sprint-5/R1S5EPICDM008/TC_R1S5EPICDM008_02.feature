#Test Case: TC_R1S5EPICDM008_02
#PBI: R1S5EPICDM008
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM Screen 2 - Association 

 
  Scenario: To validate if the DM user is able to add an individual as associated party via express addition
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM Screen-2 #using url
 	And I provide value for "T24 CIF ID" field of the migrated legal entity for which associations need to be added
 	And I click on "SEARCH" button
 	And I assert the migrated legal entity is fetched and shown in the result grid
 	And I click on button "Add Selected"
 	And I am redirected to "Capture Hierarchy Details" screen
 	And I assert the migrated entity is visible in the hierarchy
 	And I click actions (...) button from the legal entity 
 	And I click "Add Associated Party" link
 	And I am redirected to "Associated Parties" screen #To be checked during execution
 	And I validate the following fields under "Selected Entity" section
   | Label                   | FieldType        | Visible | Editable | Mandatory | Field Defaults To         		|
   | Add Associated Party to | Aplphanumeric    | true    | false    | true      | "Name of migrated entity" 		|
   | Legal Entity Type       | Aplphanumeric    | true    | false    | true      | "LE type of migrated entity" |
	And I choose value "Individual" from "Legal Entity Type" dropdown under "Associated Party Details" section 
  And I validate only the following fields are seen under "Associated Party Details" section 
   | Label                   | FieldType        | Visible | Editable | Mandatory | Field Defaults To				|
   | T24 CIF ID              | Aplphanumeric    | true    | true	   | false     | NA              					|
   | Legal Entity ID         | Aplphanumeric    | true    | true     | false     | NA               				|
   | Legal Entity Type       | Drop-down        | true    | true     | true  	   | Individual (as selected)	|
   | Title       						 | DropDown    			| true    | true     | false     | Select...         				|
   | First Name 						 | Aplphanumeric    | true    | true     | true      | NA               				|
   | Last Name  						 | Aplphanumeric    | true    | true     | true      | NA               				|
   | Date of Birth					 | Date             | true    | true     | false     | NA               				|
   | Nationality 						 | DropDown    			| true    | true     | false     | Select...         				|
   | Other Nationality			 | DropDown    			| true    | true     | false     | Select...         				|
   | Employee Status				 | DropDown    			| true    | true     | false     | Select...         				|
   | Place of Birth					 | DropDown    			| true    | true     | false     | Select...         				|
   | Gender     						 | DropDown    			| true    | true     | false     | Select...         				|
 	And I provide data for mandatory fields
  And I click on "CREATE NEW LE" button
  And I am redirected to "Association Details" screen of added associated party
  And I fill data for mandatory fields
  And I click on "SAVE & ADD ANOTHER" button
  And I assert that the associated party is added in the hierarchy successfully
  
Scenario: Validate behaviour of fields in "Association Details" screen and To validate if the DM user is able to add an already existing individual as associated party
 #Note - Continue from the previous scenario 
 	And I am redirected to "Associated Parties" screen #To be checked during execution 
  And I choose value "Individual" from "Legal Entity Type" dropdown under "Associated Party Details" section 
 	And I provide data for few mandatory fields
  And I click on "SEARCH" button
  And I assert associated party is fetched and shown in the result grid
  And I select the associated party from the result grid #using radio button
  And I click on "ADD SELECTED" button
  And I validate the only following fields in "Association Details" section
		|Fenergo Label Name								|Field Type					|Visible	|Editable	|Mandatory	|Field Defaults To 	|
		|Assoication Type 								|Drop-down					|Yes			|Yes			|Yes				|Select...  				|
		|Type of Control             			|Drop-down					|Yes			|Yes			|Yes        |Select...          |
		|Legal Basis for Data Processing 	|Drop-down					|Yes			|Yes			|Yes        |Select...          |
	And I validate LOVs of "Assoication Type" field
	#Refer Assoication PBI020 (FENERGOFAB-224) for LOV list	
	And I validate LOVs of "Type of Control" field
	#Refer Assoication PBI020 (FENERGOFAB-224) for LOV list	
	And I validate LOVs of "Legal Basis for Data Processing" field
	|Legitimate Interest|
	|Necessary for compliance with a legal obligation|
	|Necessary for performance of a contract|
	|Not Applicable|	
	And I validate "Is BOD?" field is not visible
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I validate "Shareholding %" field is not visible
	And I select "Director" for "Assoication Type" field
	And I validate "Is BOD?" field is visible
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Is BOD?           			|Drop-down		|Yes			|Yes				|Yes				|Select...					|
	And I validate LOVs of "Is BOD?" field
		|Yes|
		|No |
	And I select "Yes" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is visible
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name							|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Is Greater than 5% Shareholder?	|Alphanumeric	|Yes			|Yes				|Yes				|NA									|
	And I select "Has Shareholder" for "Assoication Type" field
	And I validate "Shareholding %" field is visible
	#This field should be displayed below "Assocation Type" field
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Shareholding %					|Numeric			|Yes			|Yes				|No					|NA									|  
	And I select "CEO" for "Assoication Type" field
  And I validate "Is BOD?" field is not visible
  And I validate "Is Greater than 5% Shareholder?" field is not visible
  And I validate "Shareholding %" field is not visible
  And I select "Non Executive Director" for "Assoication Type" field
  And I validate "Is BOD?" field is visible
  And I validate the conditional field in "Association Details" section
    | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
    | Is BOD?            | Drop-down  | Yes     | Yes      | Yes       | Select...         |
  And I select "No" for "Is BOD?" field
  And I validate "Is Greater than 5% Shareholder?" field is not visible
  And I select "Ultimate Beneficial Owner (UBO)" for "Assoication Type" field
  And I validate "Is BOD?" field is not visible
  And I select "Trustee" for "Assoication Type" field
  And I validate "Is BOD?" field is visible
  #Play around with different combination like above steps
  And I fill data for mandatory fields

  And I click on "SAVE & ADD ANOTHER" button
 
 Scenario: To validate if the DM user is able to add a non-individual as associated party via express addition
 #Note - Continue from the previous scenario 
 	And I am redirected to "Associated Parties" screen #To be checked during execution 
	And I provide data for mandatory fields
  And I click on "CREATE NEW LE" button
  And I am redirected to "Association Details" screen of added associated party
  And I fill data for mandatory fields
  And I click on "SAVE & ADD ANOTHER" button
  And I assert that the associated party is added in the hierarchy successfully
 