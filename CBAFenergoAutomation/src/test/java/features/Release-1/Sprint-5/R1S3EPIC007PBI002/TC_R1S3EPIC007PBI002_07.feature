#Test Case: TC_R1S3EPIC007PBI002_07
#PBI: R1S3EPIC007PBI002
#User Story ID: D1.10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Documents Search

  @Tobeautomated
  Scenario: Verify the following fields on "Document search" and "Add Document" screen of "Enrich KYC profile" stage for Onboarding maker
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: validate the following fields on document details screen
    And I validate the following fields as below:
      | Label         | Field Type   | Visible | editable | Mandatory |
      | GLCMS UID     | Numeric      | True    | True     | False     |
      | T24 CIF ID    | Numeric      | True    | True     | False     |
      | SWIFT Address | AlphaNumeric | True    | True     | False     |
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I search for document by searching using "Documentid"
    #Test-data: validate the following fields on document search screen
    And I validate the following fields as below:
      | Label         | Field Type   | Visible | editable | Mandatory |
      | GLCMS UID     | Numeric      | True    | True     | False     |
      | T24 CIF ID    | Numeric      | True    | True     | False     |
      | SWIFT Address | AlphaNumeric | True    | True     | False     |
