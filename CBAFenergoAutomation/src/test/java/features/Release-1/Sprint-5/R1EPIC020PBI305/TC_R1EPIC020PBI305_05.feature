#Test Case: TC_R1EPIC020PBI305_05
#PBI: R1EPIC020PBI305
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Enrich KYC Profile / LE Details / Verified LE Details - Anticipated Transaction Volumes (Annual in AED)

 
  Scenario: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible on "Enrich Client information" task, "LE360- LE details" , "LE360-LE Verified details" task for Onboarding maker for client Type "BBG"
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new request with FABEntityType as "BBG" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Complete the COB flow
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "OnboardingChecker"
    When I search for the "CaseId"
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    Then I login to Fenergo Application with "RM"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "FLoydKYC"
    When I search for the "CaseId"
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    Then I login to Fenergo Application with "FLoydAVP"
    When I search for the "CaseId"
    When I navigate to "FLODAVPReviewandSign-Off Grid" task
    When I complete "FLODAVPReviewandSign-Off" task
    Then I login to Fenergo Application with "BusinessUnitHead"
    When I search for the "CaseId"
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "LE360-LEdetails" task
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    When I navigate to "LE360-LEverifieddetails" task
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
