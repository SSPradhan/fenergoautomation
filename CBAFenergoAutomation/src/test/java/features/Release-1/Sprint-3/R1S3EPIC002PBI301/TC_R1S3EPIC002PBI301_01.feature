#Test Case: TC_R1S3EPIC002PBI301_01
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
@Automation
Feature: TC_R1S3EPIC002PBI301_01

  Scenario: Verify "legal Status" label is displaying as hidden under "Customer details" section "Enrich Client Profile" Stage for KYC Maker.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    #Test-data: Verify "legal Status" label is displaying as hidden (Read-only) under "Customer details" section
    #Then I see "LegalStatus" label is displaying as hidden under "Customer details" section
    Then I check that below data is not visible
    |FieldLabel|
    |Legal Status|
