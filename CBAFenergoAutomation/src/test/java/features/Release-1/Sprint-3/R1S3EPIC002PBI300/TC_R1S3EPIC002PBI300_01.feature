#Test Case: TC_R1S3EPIC002PBI300_01
#PBI: R1S3EPIC002PBI300
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI300_01

  Scenario: "Trading Entities" grid is displaying as greyed out ('+' sign dislaying at the top of the grid is disabled ) on "Capture request details" task of "New request" Stage for RM & Onboarding / KYC Maker.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    #When I navigate to "CaptureRequestDetailsFAB" task
    #Test-data: "Trading Entities" grid is displaying as disabled and user is not able to navigate to "Trading Entitties" task
    Then I verify  "TradingEntities" grid is displaying as disabled
    And + sign displaying at the Top of "TradingEntity" grid is also disabled and user is not able to navigate to "Trading Entitties" task
    
