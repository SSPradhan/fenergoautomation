Feature: TC_R1EPIC002PBI008_02


@Automation @Sample
Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (10) in Complete screen when 'Client Type' is selected as 'PCG-Entity' from the Enter entity details screen (Refer lov in the PBI)

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	#Select client Type as 'PCG-Entity'in Enter entity details screen
	When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "PCG-Entity"
	#Validate the Legal Entity Category lovs (Refer lov from the PBI) in complete screen
#	Then I verify "Legal Entity Category" drop-down values with ClientType as "PCG-Entity"
	#=Legal Entity Category Field is not visible in screen after R2/S4.
    And I check that below data is not visible
    |FieldLabel						|
    |Legal Entity Category|
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
