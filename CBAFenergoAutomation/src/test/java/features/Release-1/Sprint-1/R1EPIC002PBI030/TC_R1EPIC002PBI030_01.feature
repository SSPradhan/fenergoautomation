#Test Case: TC_R1EPIC002PBI030_01
#PBI: R1EPIC002PBI030
#User Story ID: US072
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_01

  @Automation
  Scenario: Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section for Onboarding Maker user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    And I can see "Nature of Activity / Business" label is renamed as "Nature of Business Activity" on "CaptureRequestDetails" screen
    # Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" and size (length upto 1000 characters) under "Business details" section
    And I click on "AddAddresss" button
    When I complete "Addresses" screen with key "CountryAsUAE"
    And I click on "SaveAddress" button
    And I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "Low"
    
   Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validating that the case status is closed
    When I navigate to "LE360overview" screen
    And I can see "Entity of Onboarding" label is renamed as "Booking Country" on "CaptureRequestDetails" screen
    And I click on "Booking Country" button to take screenshot
    # Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section on "LE360-overview" task
#    Then I can see "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section
#    And the value in "Nature of Activity Business" field is auto-populated same from "EnrichKYCProfileGrid" task
#    When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
#    When I navigate to "Businessdetails" section
#    # Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section on "LEVerifieddetails" task
#    Then I can see "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section
#    And the value in "Nature of Activity Business" field is auto-populated same from "EnrichKYCProfileGrid" task
