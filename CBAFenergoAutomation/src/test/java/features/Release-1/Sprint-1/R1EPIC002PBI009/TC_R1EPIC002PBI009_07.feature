#Test Case: TC_R1EPIC002PBI009_07
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

@TC_R1EPIC002PBI009_07
Scenario: Verify the new field 'DAO code' is available under User Details section of Trading Entity, Add Relationship screen 
Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I search user from "Relationship" on "CaptureRequestDetails"
	#Select relationship which is associated to RM group
	#check the DAO Code value which is linked to the RM user
    Then I can see "DAO Code" drop-down is displaying under "Userdetails" section
    And I check that below data is non mandatory
    |FieldLabel|
    |DAO Code|
    


	

	