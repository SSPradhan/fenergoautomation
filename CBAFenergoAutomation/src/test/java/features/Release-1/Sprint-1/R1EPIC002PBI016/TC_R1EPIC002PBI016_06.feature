#Test Case: TC_R1EPIC002PBI016_06
#PBI: R1EPIC002PBI016
#User Story ID: USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section


Scenario: Validate "Entity of Onboarding" dropdown is renamed as "Booking Country" drop-down under "Internal Booking details" section on "Review request" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 	
	When I navigate to "ReviewRequest" task	
	#Test-data: Verify "Entity of Onboarding" dropdown should be renamed as "Booking Country" drop-down on "Review Request" screen
	Then I can see "Entity of Onboarding" dropdown is renamed as "Booking Country" drop-down
	