#Test Case: TC_R1EPIC002PBI016_10
#PBI: R1EPIC002PBI016
#User Story ID: US057
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section

Scenario: Verify LOVs for "UID Originating Branch" drop-down under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "FI/NBFI"
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I verify LOVs for "UID Originating Branch" drop-down under "Internal Booking details"
	#Test-data: Verify LOVs for "UID Originating Branch" when client Type is "FI/NBFI"
	Then I can see LOVs for "UID Originating Branch" drop-down is defaults to "Head Office _ BNK-100" and editable