#Test Case: TC_R1EPIC002PBI010_10
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task - Anticipated Transaction Sub Flow

Scenario: Validate if "Anticipated Transaction" subflow is not available in "Enrich KYC profile" task for "FI" entity

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "FI" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "FI" and LegalEntityrole as "Client/Counterparty"  
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task 
	And I assert that "Anticipated Transactional Activity (Per Month)" subflow is not available in "Enrich KYC profile" screen
	When I complete "EnrichKYCProfileFAB" task 