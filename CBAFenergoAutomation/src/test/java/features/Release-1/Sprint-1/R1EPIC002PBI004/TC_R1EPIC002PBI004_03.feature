#Test Case: TC_R1EPIC002PBI004_03
#PBI: R1EPIC002PBI004
#User Story ID: US024
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI004_03

  @Automation @TC_R1EPIC002PBI004_03
  Scenario: 
    Verify the conditionally available field behaviour of new fields when Client type is NBFI

    Given I login to Fenergo Application with "RM:NBFI"
    #Create entity with Country of Incorporation = UAE and client type = NBFI
    When I create a new request with FABEntityType as "Non-Bank Financial Institution (NBFI)" and LegalEntityRole as "Client/Counterparty"
    And I check that below data is available
      | Label               | FieldType | Visible | Mandatory | ReadOnly | DefaultsTo |
      | Channel & Interface | Dropdown  | Yes     | Yes       | Yes      | Select...  |
