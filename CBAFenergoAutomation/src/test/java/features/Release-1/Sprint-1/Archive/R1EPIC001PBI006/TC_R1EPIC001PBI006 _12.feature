Feature: TC_R1EPIC001PBI006 _12

Scenario: Validate the field attributes of 'Assigned To' in Case search screen.
	
	Given I login to Fenergo Application with "RMUser" 
	When I navigate to "CaseSearch" screen
	And I check that below data is not mandatory 
	|FieldLabel|FieldType |Editable|Value|
	|Assigned To| Dropdown|Yes|Select�|
