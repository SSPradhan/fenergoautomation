#Test Case: TC_R1EPIC01PBI001_03
#PBI: R1EPIC01PBI008
#User Story ID: US028
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate the behavior of "TaxIdentifierValue" field for input as "15 Alphanumeric characters/Alphabets"
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	#Test Data: Tax Type: VAT ID
	When I add a "TAX Type" as "VAT ID" on "AddTaxIdentifier" screen
	When I add "TaxIdentifierValue" as "15 Alphanumeric characters/Alphabets"
	Then I can see "TaxIdentifierValue" is not accepted/saved
	