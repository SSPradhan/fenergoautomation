Feature: TC_R1EPIC001PBI006 _03

Scenario: Verify the KYCMaker user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "KYCMakerUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any KYC Maker user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the KYC Maker user
	Then I validate the search grid data
