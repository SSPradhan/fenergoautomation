Feature: TC_R1EPIC001PBI006 _13

Scenario:Validate the dropdown values of 'Assigned To' field in Case search screen.
	
	Given I login to Fenergo Application with "RMUser" 
	When I navigate to "CaseSearch" screen
	Then I validate dropdown values of AssignedTo field
	#Dropdown values should contain all the Fenergo Users
	Then I validate dropdown values of AssignedTo field
