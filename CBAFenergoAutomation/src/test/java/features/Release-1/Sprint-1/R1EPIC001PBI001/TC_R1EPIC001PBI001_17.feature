#Test Case: TC_R1EPIC01PBI001_17
#PBI: R1EPIC01PBI001
#User Story ID: US015
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS

Feature: Products Section- Grid View - Cancelled products


Scenario: Verify that the Cancelled Product is not visible in Capture Request Details and Enrich KYC profile screen for Business Unit Head (N3), FLOD VP user,Onboarding Checker and Business Head (N2) once the stage moved to review and sign off 
	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with ClientEntityType as "Corporate" and ClientEntityRole as "Client/Counterparty" 
	When I add a product in capture request details page 
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Then I login to Fenergo Application with "KYCManager" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatory" task 
	Then I navigate to "EnrichKYCProfileSection" task 
	When I cancel a product in EnrichKYCProfile Screen 
	Then I can see the Cancelled product is visible in the product grid with status as "Cancelled" 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task
	When I complete "RiskAssessment" task
	
	Then I login to Fenergo Application with "BusinessUnitHead(N3)" 
	Then I navigate to "CaptureRequestDetails" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "ReviewRequest" task 
	Then I cannot see the Cancelled product is visible in the product grid
	Then I navigate to "ValidateKYCandRegulatory" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "EnrichKYCProfile" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	
	Then I login to Fenergo Application with "Flod VP" 
	Then I navigate to "CaptureRequestDetails" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "ReviewRequest" task 
	Then I cannot see the Cancelled product is visible in the product grid
	Then I navigate to "ValidateKYCandRegulatory" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "EnrichKYCProfile" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	
	Then I login to Fenergo Application with "BusinessHead(N2)" 
	Then I navigate to "CaptureRequestDetails" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "ReviewRequest" task 
	Then I cannot see the Cancelled product is visible in the product grid
	Then I navigate to "ValidateKYCandRegulatory" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "EnrichKYCProfile" task 
	Then I cannot see the Cancelled product is visible in the product grid  
	
	Then I login to Fenergo Application with "OnboardingChecker" 
	Then I navigate to "CaptureRequestDetails" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "ReviewRequest" task 
	Then I cannot see the Cancelled product is visible in the product grid
	Then I navigate to "ValidateKYCandRegulatory" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	Then I navigate to "EnrichKYCProfile" task 
	Then I cannot see the Cancelled product is visible in the product grid 
	
	
	
	
	
	