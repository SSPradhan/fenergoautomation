#Test Case: TC_R1EPIC01PBI001_03
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Products Section- Grid View

  @COB @Automation
  Scenario: Validate if Onboarding Maker is able to see "Product Status" as a column on the "Products grid view" in "Enrich KYC Profile" screen after adding the Product
    Given I login to Fenergo Application with "RM:NBFI"
    #=Creating a legal entity with "Client Entity Type" as "NFI" and "Legal Entity Role" as "Client/Counterparty "
		When I complete "NewRequest" screen with key "NBFI" 
		And I add a Product from "CaptureRequestDetails"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button  
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
		When I complete "ValidateKYC" screen with key "NBFI" 
		And I click on "SaveandCompleteforValidateKYC" button 
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "EnrichKYCProfile"
    And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile"
    #And I add a Product from "EnrichKYCProfile"
