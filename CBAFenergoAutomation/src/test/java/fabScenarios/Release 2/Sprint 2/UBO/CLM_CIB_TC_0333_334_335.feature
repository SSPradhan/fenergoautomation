Feature: SCN_RR_027

@Automation 
Scenario: 
Given I login to Fenergo Application with "RM:NBFI" 
	When I complete "NewRequest" screen with key "NBFI" 
	And I complete "CaptureNewRequest" with Key "NBFI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
    When I complete "AssociatedPartyShareHolder24UBO" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociatedPartyShareHolder76" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button 
     When I complete "CaptureHierarchyDetails" task 	
    When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I verify the populated risk rating is "Low" 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed"
	
	#Regular review
	And I initiate "Regular Review" from action button 
	And I navigate to "CloseAssociatedCasesGrid" task
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	And I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid
	When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
    When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
     When I complete "AssociatedPartyShareHolder24UBO" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button 
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task