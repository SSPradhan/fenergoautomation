#Test Case: TC1_CLM_CIB_TC_0338
#User Story ID: SCN_RR_029
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC1_CLM_CIB_TC_0338_To Verify that under Industry Code Dropdowns

  @Automation
  Scenario: To Verify that under Industry Code Details Primary Industry of Operation UAE field LOV '00005-Trusts' is replaced with 00005-Individual and Trusts
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    And I verify "Primary Industry of Operation" drop-down values
    And I verify "Primary Industry of Operation UAE" drop-down values
    And I verify "Secondary Industry of Operation" drop-down values
    
    