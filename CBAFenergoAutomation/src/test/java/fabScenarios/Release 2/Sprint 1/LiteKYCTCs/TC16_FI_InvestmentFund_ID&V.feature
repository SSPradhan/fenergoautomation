#Test Case: TC_16_Investment Fund
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC16_Investment Fund_FI_ID&V Feature

  @Automation
  Scenario: Verify KYC Maker is able add association on ID&V Task when Client Type is "FI" and Category "Investment Fund" and also able to add Address, Documents,Identifier
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Investment Fund" for "Dropdown" field "Legal Entity Category"
   
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Investment Fund" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI Investment Fund"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
	  When I complete "EditID&V" task   
	  When I complete "AddressAddition" in "Edit Verification" screen
	  When I complete "Documents" in "Edit Verification" screen
	  When I complete "TaxIdentifier" in "Edit Verification" screen
	  When I complete "LE Details" in "Edit Verification" screen
	  When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
