#Test Case: TC01_NBFI_NonVostroNonCasa : ETE validation
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC01_NBFI_NonVostroNonCasa : ETE validation

  @Automation
  Scenario: Validate ETE LiteKYC flow for Client Type "NBFI"  and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    Given I login to Fenergo Application with "RM:NBFI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Non-Bank Financial Institution (NBFI)"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    And I check that below data is visible
      | FieldLabel                                 |
      | Does the CDD profile qualify for Lite KYC? |
    And I validate the following fields in "Complete" Screen
      | Label                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Does the CDD profile qualify for Lite KYC? | Dropdown  | true    | false    | true      | Select...  |
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    When I navigate to "LE360overview" screen
    When I navigate to "Cases" from LHN section
    Then I Validate the CaseName Contains "Lite KYC Onboarding" in it   
    And I navigate to "CaptureRequestDetailsGrid" task
    Then I verify that workflow name is "Lite KYC Onboarding" in "ReviewRequest" task
    When I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    Then I verify that workflow name is "Lite KYC Onboarding" in "ReviewRequest" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I verify that workflow name is "Lite KYC Onboarding" in "Validate KYC and Regulatory Data" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    Then I verify that workflow name is "Lite KYC Onboarding" in "Enrich KYC Profile" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    Then I verify that workflow name is "Lite KYC Onboarding" in "Capture Hierarchy Details" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I assert below documents are present with following properties
      | KYC Document Requirement                                                                     | Default Document Type                                                                        | Default Document Category | Mandatory |
      | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Constitutive              | Yes       |
      | Wolfsberg Questionnaire                                                                      | Wolfsberg Questionnaire                                                                      | AOF                       | No        |
      | Others                                                                                       | Others                                                                                       | MISC                      | No        |
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    Then I check that the "RiskAssessmentLiteKYC" button is disabled
    And I assert "RiskAssessmentLiteKYC" is populated as "Medium"
    When I complete "RiskAssessment" screen with key "LiteKYC"
    
    Then I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    
    
